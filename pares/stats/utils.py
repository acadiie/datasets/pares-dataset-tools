#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT


from typing import Any, List

import numpy as np


def flatten_list(list_to_flatten: List[List[Any]]) -> List[Any]:
    """
    This function will flatten a list of lists.

    :param list_to_flatten: The list of lists to flatten
    :return: The flattened list
    """
    return [item for sublist in list_to_flatten for item in sublist]


def count_outliers(data: List[int] | List[float]) -> int:
    """
    Simple outlier detection using the IQR method

    :param data: The data to find the outliers in (must be a list of ints or floats)
    :return: The number of outliers in the data
    """
    # Calculate the quartiles and IQR
    q1, q3 = np.percentile(data, [25, 75])
    iqr = q3 - q1
    # Calculate the lower and upper bounds for outliers
    lower_bound = q1 - 1.5 * iqr
    upper_bound = q3 + 1.5 * iqr
    # Identify the outliers
    outliers = [x for x in data if x < lower_bound or x > upper_bound]
    # Calculate the number of outliers
    num_outliers = len(outliers)

    return num_outliers
