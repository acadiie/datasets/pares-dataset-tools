#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT


from typing import Any, Dict, List, Tuple

import cv2
import numpy as np
from shapely.geometry import Polygon


def find_box_pixel_loss(lines: List[Dict[str, Any]]) -> float:
    """
    This function will find the average percentage of pixels lost by converting
    the polygon to a bounding box then comparing the area of the polygon to the
    area of the bounding box.

    :param lines: A list of lines from the arkindex api, this comes from an arkindex query and some flattening
    :return: The average percentage of pixels lost by converting the polygon to a bounding box for a single
                 list of line's polygons
    """

    polys = [Polygon(line["zone"]["polygon"]) for line in lines]
    losses = []
    # find the bounding box of each polygon
    bounding_boxes = [polygon.bounds for polygon in polys]
    # compare the bouding box to the polygon to find how much loss conversion to a bounding box would cause
    for poly, bounding_box in list(zip(polys, bounding_boxes)):
        # find the area of the polygon using numpy so that the area is in pixels
        # make an np array the size of the bounding box around the polygon
        # buffer is needed to make sure the polygon is not cut off
        buffer = 10
        mask = np.zeros(
            (int(bounding_box[3] - bounding_box[1]) + buffer, int(bounding_box[2] - bounding_box[0]) + buffer)
        )
        # plot the points of the exterior of the polygon on the mask
        x, y = poly.exterior.xy
        x = x - np.array(bounding_box[0])
        y = y - np.array(bounding_box[1])
        # polyfill the mask
        pts = np.array([np.array([x, y]).T], dtype=np.int32)
        cv2.fillPoly(mask, pts, 1)
        # find the area of the polygon
        poly_area = np.sum(mask)
        # find the area of the bounding box
        bounding_box_area = (bounding_box[2] - bounding_box[0]) * (bounding_box[3] - bounding_box[1])
        # find the loss of area to show how many pixels are lost by using the bounding box rather than the polygon
        loss = poly_area - bounding_box_area
        # find the percentage of loss
        loss_percentage = abs(loss / poly_area)
        losses.append(loss_percentage)
    return np.mean(losses)


def conglom_mask_and_pix_stats(
    pages: List[Dict[str, Any]], page_lines: List[List[Dict[str, Any]]]
) -> Tuple[np.array, float, float]:
    """
    This function will create a mask that is the size of the largest given page
    and add all of the masks of the lines to it. It will also find the average
    number of active pixels per page and the average page coverage percentage of
    the polygons on on the pages.

    :param pages: A list of pages from the arkindex api
    :param page_lines: A list of lists of lines from the arkindex api, this comes from an arkindex
                         query and some flattening
    :return: A tuple containing the conglomerate mask, the average number of active pixels per page
                , and the average page coverage percentage of the polygons on on the pages
    """
    # Calculate the maximum dimensions of the mask
    max_height = max(page["zone"]["image"]["height"] for page in pages)
    max_width = max(page["zone"]["image"]["width"] for page in pages)

    # Create a new mask array with the maximum dimensions
    conglomerate_mask = np.zeros((max_height, max_width), dtype=np.uint8)
    # also find the average number of active pixels per page
    active_pixels = []
    # and average page coverage percentage based on the number of active pixels
    page_coverage = []
    for page, lines in zip(pages, page_lines):
        polys = [np.array(line["zone"]["polygon"], dtype=np.int32) for line in lines]
        # make an np array filled with zeros the size of the page
        mask = np.zeros((page["zone"]["image"]["height"], page["zone"]["image"]["width"]), dtype=np.uint8)
        cv2.fillPoly(mask, polys, 1)
        active_pixels.append(np.sum(mask))
        page_coverage.append(np.sum(mask) / (mask.shape[0] * mask.shape[1]))
        # pad all sides of the mask with zeros so that it is the same size as the conglomerate_mask
        x_pad = max_width - mask.shape[1]
        y_pad = max_height - mask.shape[0]
        mask = np.pad(
            mask,
            ((y_pad // 2, y_pad - y_pad // 2), (x_pad // 2, x_pad - x_pad // 2)),
            mode="constant",
            constant_values=0,
        )

        conglomerate_mask = np.sum([conglomerate_mask, mask], axis=0)
    average_active_pixels = np.mean(active_pixels)
    average_page_coverage = np.mean(page_coverage)
    return conglomerate_mask, average_active_pixels, average_page_coverage
