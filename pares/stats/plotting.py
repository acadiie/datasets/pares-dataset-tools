#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT


from pathlib import Path
from typing import Dict, Optional, Tuple

import matplotlib.pyplot as plt
import numpy as np


def save_image_of_numpy_array(array: np.array, output_folder: Path, name: str, cmap: str) -> None:
    """
    This function will save an image of the numpy array at the path given by the user.

    :param array: The numpy array to save as an image
    :param output_folder: The path to the folder where to save the image
    :param name: The name of the image save without the extension
    :param cmap: The color map to use for the image, only colormaps that work with matplotlib.pyplot.imshow work
    :return: None
    """
    plt.imshow(array, cmap=cmap)
    plt.savefig((output_folder / (name + ".png")), bbox_inches="tight")
    plt.close()


def save_boxplot_of_single_numpy_array(
    array: np.array,
    output_folder: Path,
    name: str,
    xlabel: Optional[str] = None,
    figsize: Optional[Tuple[float, float]] = None,
) -> None:
    """
    This function will save a boxplot of the numpy array at the path given by the user.

    :param array: The numpy array to save as a boxplot image
    :param output_folder: The path to the folder where to save the image
    :param name: The name of the image save without the extension
    :param xlabel: The label for the x axis
    :param figsize: The size of the figure
    :return: None
    """
    fig, ax = plt.subplots(figsize=figsize if figsize else (10, 10))
    ax.boxplot(array, vert=False)
    # remove all ticks on the y axis if yticks is None or empty else use the yticks
    ax.set_yticks([])
    ax.set_xlabel(xlabel if xlabel else "")
    ax.set_title(name)
    fig.tight_layout()
    plt.savefig((output_folder / (name + ".png")), bbox_inches="tight")
    plt.close()


def ask_for_order() -> Optional[str]:
    """
    This function will ask the user to put in the order for the chart.
    For the make_chart_from_counter_with_prompts function.

    :return: The order for the chart (typically only used with the make_chart_from_counter_with_prompts function)
    """
    # get the order for the chart
    while True:
        order: Optional[str] = None
        order = str(input("Please put in the order for the chart (asc/desc/none): "))
        if order.casefold() == "none" or order.casefold() == "":
            order = None
        if str(order).casefold() in ["asc", "desc"] or order is None:
            break
        print("Please put in a valid order (asc/desc/none(could be empty))")
    return order


def ask_for_limit(counter: Dict) -> Optional[int]:
    """
    This function will ask the user to put in the limit for the chart.
    For the make_chart_from_counter_with_prompts function.

    :param counter: The counter to get the limit for, as to ensure the limit is not greater
                         than the length of the counter
    :return: The limit for the chart (typically only used with the make_chart_from_counter_with_prompts function)
    """
    # get the limit for the chart
    while True:
        limit: Optional[str | int] = None
        limit = str(input("Please put in the number of items to show (int or none): "))

        if limit.casefold() == "none" or limit.casefold() == "":
            limit = None
        limit = int(limit) if limit else None

        if limit is not None and (limit < 0 or limit > len(counter)):
            print("Please put in a valid number (int or none(could be empty))")
            print(f"Number must be between 0 and {len(counter)}")
        if isinstance(limit, int) or limit is None:
            break

        print("Please put in a valid limit (int or none(could be empty))")
    return limit


def make_chart_from_counter_with_prompts(counter: Dict) -> plt.Figure:
    """
    This function will make a chart from the counter.
    It take user input for the title, order, and limit.

    :param counter: The counter to make the chart from, typically the Dict form of a Counter
                     from the collections module
    :return: The figure and axis of the chart to be showed or saved
    """
    # prompt the user to put in a title for the chart
    title = str(input("Please put in a title for the chart: "))

    order = ask_for_order()

    limit = ask_for_limit(counter)

    # order the counter order and limit the counter to the given limit
    if order == "asc":
        counter = dict(sorted(counter.items(), key=lambda item: item[1]))
    elif order == "desc":
        counter = dict(sorted(counter.items(), key=lambda item: item[1], reverse=True))

    counter = dict(list(counter.items())[: int(limit if limit is not None else len(counter))])

    fig, ax = plt.subplots()
    ax.bar(counter.keys(), counter.values())
    ax.set_xlabel("Key")
    ax.set_title(title)
    fig.tight_layout()
    return fig, ax
