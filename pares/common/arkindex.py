#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

"""
Wrapper around the ArkIndex tools provided by TEKLIA to access the PARES Dataset.
"""
import logging
import os
from collections import OrderedDict
from concurrent.futures import ThreadPoolExecutor
from dataclasses import dataclass
from functools import lru_cache
from pathlib import Path
from typing import Any, Collection, Dict, List, Optional, Tuple

import shapely.geometry
from apistar.exceptions import ErrorResponse
from arkindex import ArkindexClient
from dotenv import load_dotenv
from shapely import Polygon


@dataclass
class Entity:
    name: str
    type: str
    offset: int
    length: int


class APIException(Exception):
    """
    Exception with the ArkIndexAPI endpoints.
    """


class ArkIndexAPI:
    # region Common methods for the ArkIndex API
    @staticmethod
    def create_from_environment():
        load_dotenv()
        return ArkIndexAPI(
            os.environ.get("ARKINDEX_API_URL"),
            os.environ.get("ARKINDEX_API_TOKEN"),
            os.environ.get("ARKINDEX_USER_WORKER_RUN"),
        )

    def _request(self, endpoint, *args, **kwargs) -> Dict:
        try:
            return self._internal_api.request(endpoint, *args, **kwargs)
        except ErrorResponse as er:
            logging.critical("Error requesting with %s (id: %s)", endpoint, kwargs.get("id"))
            raise APIException(f"Error requesting {endpoint} with and {kwargs}") from er

    def _paginate(self, endpoint, *args, **kwargs) -> Collection[Dict]:
        try:
            return self._internal_api.paginate(endpoint, *args, **kwargs)
        except ErrorResponse as er:
            logging.critical("Error paginating with %s (id: %s)", endpoint, kwargs.get("id"))
            raise APIException(f"Error paginating {endpoint} with and {kwargs}") from er

    def __init__(self, arkindex_api_url: str, arkindex_api_token: str, worker_run_uuid: str):
        self._internal_api = ArkindexClient(base_url=arkindex_api_url, token=arkindex_api_token)
        self._worker_run_uuid = worker_run_uuid

    # endregion

    # region Crud methods

    # region Retrieval methods
    @lru_cache(maxsize=50)
    def get_element_json_from_element_uuid(self, element_uuid: str) -> Dict:
        return self._request("RetrieveElement", id=element_uuid)

    def get_element_name_from_element_uuid(self, element_uuid: str) -> str:
        return self.get_element_json_from_element_uuid(element_uuid).get("name", "")

    # endregion

    # region Deletion methods
    def delete_entity_type(self, entity_type_uuid: str):
        self._request("DestroyEntityType", id=entity_type_uuid)

    def delete_transcription(self, transcription_uuid: str):
        self._request("DestroyTranscription", id=transcription_uuid)

    def delete_entity(self, entity_uuid: str):
        self._request("DestroyEntity", id=entity_uuid)

    # endregion

    # region List methods
    # region List methods with specified worker UUIDs

    def list_transcriptions_of_text_line(self, text_line_uuid: str):
        return list(self._paginate("ListTranscriptions", id=text_line_uuid, worker_run=self._worker_run_uuid))

    def list_page_transcriptions_in_a_folder(self, folder_uuid: str) -> List:
        return list(
            self._paginate(
                "ListTranscriptions",
                id=folder_uuid,
                recursive=True,
                element_type="page",
                worker_run=self._worker_run_uuid,
            )
        )

    def list_transcription_entities_of_transcription(self, element_uuid: str) -> List:
        return list(
            self._paginate("ListTranscriptionEntities", id=element_uuid, entity_worker_run=self._worker_run_uuid)
        )

        # endregion
        # region List methods without specified UUIDs

    def list_corpus_entity_types(self, corpus_uuid: str) -> List:
        return list(self._paginate("ListCorpusEntityTypes", id=corpus_uuid))

    def list_corpus_entities(self, corpus_uuid: str) -> List:
        return list(self._paginate("ListCorpusEntities", id=corpus_uuid))

    def list_element_children(self, element_uuid: str) -> List:
        return list(self._paginate("ListElementChildren", id=element_uuid, allow_missing_data=True))

    def list_element_child_names(self, element_uuid: str) -> List:
        data = self.list_element_children(element_uuid)
        return [element.get("name") for element in data]

    def list_element_children_with_specific_class(self, element_uuid: str, class_uuid: Optional[str] = None) -> List:
        return list(
            self._paginate("ListElementChildren", id=element_uuid, class_id=class_uuid, allow_missing_data=True)
        )

    def list_element_children_with_classes(self, element_uuid: str) -> List:
        return list(self._paginate("ListElementChildren", id=element_uuid, with_classes=True))

    def list_page_text_lines(self, page_uuid: str):
        """
        Return the text lines contained in a page, sorted according to the Y axis, from top to bottom.
        """
        return sorted(
            self._paginate("ListElementChildren", id=page_uuid, type="text_line"),
            key=lambda line: Polygon(line["zone"]["polygon"]).centroid.y,
        )

    def list_all_parts_of_table(self, page_uuid: str) -> List:
        """
        List all the parts that exist in the image.
        """
        text_lines = self._paginate("ListElementChildren", id=page_uuid, type="text_line")
        # there is text that belongs with the text lines but are not in line some below lines some above
        # get the above ones
        above_text_lines = self._paginate("ListElementChildren", id=page_uuid, type="above_text_line")
        # get the below ones
        below_text_lines = self._paginate("ListElementChildren", id=page_uuid, type="sub_text_line")

        comments = self._paginate("ListElementChildren", id=page_uuid, type="comment")

        page_headers = self._paginate("ListElementChildren", id=page_uuid, type="page_header")

        page_footers = self._paginate("ListElementChildren", id=page_uuid, type="page_footer")

        table_headers = self._paginate("ListElementChildren", id=page_uuid, type="table_header")

        all_text = (
            list(page_headers)
            + list(table_headers)
            + list(text_lines)
            + list(above_text_lines)
            + list(below_text_lines)
            + list(page_footers)
            + list(comments)
        )
        # sort the text lines by their centroid y
        sorted_text = sorted(all_text, key=lambda line: Polygon(line["zone"]["polygon"]).centroid.y)
        return sorted_text
        # endregion

    # endregion

    # region Creation methods
    def create_classification_on_image(self, image_uuid: str, classification_name: str):
        self._request(
            "CreateClassification",
            body={"element": image_uuid, "ml_class": classification_name},
        )

    def create_entity_type_on_corpus(self, corpus_uuid: str, entity_type_name: str, entity_type_color: str):
        self._request(
            "CreateEntityType",
            body={"name": entity_type_name, "corpus": corpus_uuid, "color": entity_type_color},
        )

    def create_page_transcription_with_entities(
        self, corpus_uuid: str, page_uuid: str, transcription: str, entities: List[Entity]
    ):
        created_transcription = self._create_transcription_for_element(page_uuid, transcription)
        corpus_entities = {x["name"]: x["id"] for x in self.list_corpus_entity_types(corpus_uuid)}
        self._create_transcription_entities_for_transcription(
            created_transcription.get("id"), corpus_entities, entities
        )

    def create_line_transcription_with_entities(
        self, corpus_uuid: str, text_line_uuid: str, transcription: str, entities: List[Entity]
    ):
        created_transcription = self._create_transcription_for_element(text_line_uuid, transcription)
        corpus_entities = {x["name"]: x["id"] for x in self.list_corpus_entity_types(corpus_uuid)}
        self._create_transcription_entities_for_transcription(
            created_transcription.get("id"), corpus_entities, entities
        )

    def _create_transcription_for_element(self, element__uuid: str, transcription: str):
        return self._request(
            "CreateTranscription",
            id=element__uuid,
            body={"text": transcription, "confidence": 1.0, "worker_run_id": self._worker_run_uuid},
        )

    def _create_transcription_entities_for_transcription(
        self, transcription_uuid: str, corpus_entities: Dict, entities: List[Entity]
    ):
        self._request(
            "CreateTranscriptionEntities",
            id=transcription_uuid,
            body={
                "worker_run_id": self._worker_run_uuid,
                "entities": [
                    {
                        "name": entity.name,
                        "type_id": corpus_entities[entity.type],
                        "offset": entity.offset,
                        "length": entity.length,
                        "confidence": 1.0,
                    }
                    for entity in entities
                ],
            },
        )

    # endregion

    # endregion

    # region Other methods
    def map_images_names_and_uuids_from_within_a_folder(self, folder_uuid: str) -> OrderedDict:
        """
        Get all the filenames and UUID of the files contained in a given folder on ArkIndex.
        """
        pages = list(self._paginate("ListElementChildren", id=folder_uuid))
        page_uuids = [page["id"] for page in pages]
        page_names = [f"{page['name']}.png" for page in pages]
        return OrderedDict(zip(page_names, page_uuids))

    # endregion


# region Helper functions for the PARES dataset
def __get_map_of_csv_names_with_page_uuid_and_selected_csv_files(
    page_uuids_names: Dict[str, str],
    path_to_image_cvs_folder: Path,
) -> Tuple[Dict[str, str], List[Path]]:
    """
    Get a dict of all the CSV names (as key) with their UUID on Arkindex plus all the selected
    CSV files based on that selection.

    :param page_uuids_names: Dict of page UUID as keys and page names as values
    :param path_to_image_cvs_folder: Path to the folder containing the CSV files
    :return: Tuple of a dict of all the CSV names (as key) with their UUID on Arkindex plus all the selected
    """
    # Load CSV files that are within this folder
    # Page names contain the CSV filename, for instance: Echevronne_1724__echevronne_1670_1829_0071
    # After the ‘__’, it’s the original CSV filename, lowered, without prefix.
    map_page_uuid_csv_name = dict(
        zip(page_uuids_names.keys(), [i.split("__")[1].lower() for i in page_uuids_names.values()])
    )
    map_page_csv_name_uuid = {v: k for k, v in map_page_uuid_csv_name.items()}
    csv_files = [
        path
        for path in path_to_image_cvs_folder.glob("**/*.csv")
        if path.stem.lower() in map_page_uuid_csv_name.values()
    ]
    return map_page_csv_name_uuid, csv_files


def __get_map_page_uuids_page_names(api: ArkIndexAPI, arkindex_image_folder_uuid: str):
    """
    Get a dict of page UUID as keys and page names as values

    :param api: ArkIndexAPI instance
    :param arkindex_image_folder_uuid: UUID of the folder containing the images on ArkIndex
    :return: Dict of page UUID as keys and page names as values
    """
    pages = api.list_element_children(arkindex_image_folder_uuid)
    return {page.get("id"): page.get("name") for page in pages}


def map_page_uuids_with_csv_files(
    api: ArkIndexAPI, path_to_image_cvs_folder: Path, arkindex_image_folder_uuid: str
) -> Dict[str, Path]:
    """
    Get a map of Page UUID in a folder on ArkIndex with the path of the corresponding CSV file on the filesystem.

    :param api: ArkIndexAPI instance
    :param path_to_image_cvs_folder: Path to the folder containing the CSV files
    :param arkindex_image_folder_uuid: UUID of the folder containing the images on ArkIndex
    :return: Dict of page UUID as keys and page names as values
    """
    page_uuids_names = __get_map_page_uuids_page_names(api, arkindex_image_folder_uuid)
    map_page_name_id, csv_files = __get_map_of_csv_names_with_page_uuid_and_selected_csv_files(
        page_uuids_names, path_to_image_cvs_folder
    )
    return dict(zip([map_page_name_id[csv_file.stem.lower()] for csv_file in csv_files], csv_files))


def map_page_name_with_csv_files(
    api: ArkIndexAPI, path_to_image_cvs_folder: Path, arkindex_image_folder_uuid: str
) -> Dict[str, Path]:
    """
    Get a map of Page name in a folder on ArkIndex with the path of the corresponding CSV file on the filesystem.

    :param api: ArkIndexAPI instance
    :param path_to_image_cvs_folder: Path to the folder containing the CSV files
    :param arkindex_image_folder_uuid: UUID of the folder containing the images on ArkIndex
    :return: Dict of page UUID as keys and page names as values
    """
    page_uuids_names = __get_map_page_uuids_page_names(api, arkindex_image_folder_uuid)
    map_page_name_id, csv_files = __get_map_of_csv_names_with_page_uuid_and_selected_csv_files(
        page_uuids_names, path_to_image_cvs_folder
    )
    uuid_name = dict(zip([map_page_name_id[csv_file.stem.lower()] for csv_file in csv_files], csv_files))
    return {page_uuids_names[uuid]: uuid_name[uuid] for uuid in page_uuids_names.keys()}


def map_text_lines_to_row_numbering(
    api: ArkIndexAPI, arkindex_image_folder_uuid: str
) -> Dict[str, List[Dict[Any, Any]]]:
    """
    Get a map of text line UUIDs with their transcriptions.

    :param api: ArkIndexAPI instance
    :param arkindex_image_folder_uuid: UUID of the folder containing the images on ArkIndex
    :return: Dict of text line UUIDs with their transcriptions

    Example of the output:
    {
        'uuid_str':  row_number,
        ...
    }
    """
    # first get a map of page UUIDs with their names
    page_uuids_names = __get_map_page_uuids_page_names(api, arkindex_image_folder_uuid)
    print(page_uuids_names)
    #
    page_uuids_to_text_lines: Dict[str, List[Dict]] = {}
    with ThreadPoolExecutor() as executor:
        # get a list of jobs to do in parallel
        jobs = [(api.list_page_text_lines, page_uuid) for page_uuid in page_uuids_names.keys()]
        # execute the jobs in parallel
        results = executor.map(lambda job: job[0](job[1]), jobs)
        # get the results
        for page_name, text_lines in zip(page_uuids_names.values(), results):
            page_uuids_to_text_lines[page_name] = text_lines

    return page_uuids_to_text_lines


def get_train_test_validation_with_image_names(
    api: ArkIndexAPI, train_folder_uuid: str, test_folder_uuid: str, validation_folder_uuid: str, fast: bool = False
) -> Tuple[List[str], List[str], List[str]]:
    """
    Get a list of image names for the train, test and validation folders.

    :param api: ArkIndexAPI instance
    :param train_folder_uuid: UUID of the train folder
    :param test_folder_uuid: UUID of the test folder
    :param validation_folder_uuid: UUID of the validation folder
    :return: Tuple of train, test and validation image names in that order
    """
    if not fast:
        train_images = api.list_element_child_names(train_folder_uuid)
        test_images = api.list_element_child_names(test_folder_uuid)
        validation_images = api.list_element_child_names(validation_folder_uuid)
    # instead of doing this seqentially, we could do it in parallel using multithreading
    if fast:

        def _execute_function(job):
            function, *args = job
            return function(*args)

        # create a list of jobs to do in parallel
        jobs = [
            (api.list_element_child_names, train_folder_uuid),
            (api.list_element_child_names, test_folder_uuid),
            (api.list_element_child_names, validation_folder_uuid),
        ]
        # create a thread pool
        with ThreadPoolExecutor() as executor:
            # execute the jobs in parallel
            results = executor.map(_execute_function, jobs)
            # get the results
            train_images, test_images, validation_images = list(results)

    return train_images, test_images, validation_images


def order_text_lines_by_vert_center(text_lines: List[Dict]) -> List[Dict]:
    """
    Order text lines by their vertical center.

    :param text_lines: List of text lines (in dict format from ArkIndex)
    :return: Ordered list of text lines
    """
    sorted_text_lines = sorted(
        text_lines, key=lambda line: shapely.geometry.Polygon(line["zone"]["polygon"]).centroid.y
    )
    return sorted_text_lines


# endregion
