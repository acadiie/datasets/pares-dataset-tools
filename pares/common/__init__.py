#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

import json
from pathlib import Path
from typing import Any, Dict


def read_config_file_json(config_file_path: Path) -> Dict[str, Any]:
    with open(config_file_path, mode="r", encoding="utf-8") as input_config_file:
        return json.load(input_config_file)
