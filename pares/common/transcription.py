#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

"""
Transcription of the PARES Dataset, usually stored as CSV files.
"""

import html
import math
import random
from pathlib import Path
from typing import Any, Dict, Generator, List, Optional, Tuple

import numpy as np
import pandas
from pandas import DataFrame

from pares.common.arkindex import Entity


class CSVTranscription:
    """
    A single CSV transcription.
    """

    RANGE_COLUMNS = (0, 12)
    INDEX_COLUMN_IDENTITY = 13

    def __init__(self, path_to_csv_transcription: Path):
        self._transcriptions = list(Path(path_to_csv_transcription).glob("**/*.csv"))
        self.transcription: DataFrame = pandas.read_csv(path_to_csv_transcription, dtype=object)
        self.transcription = self.transcription.drop(["nom de la paroisse", "date année"], axis=1)

    @property
    def annotated_elements(self) -> List[str]:
        """The elements annotated in the transcription. These are the columns of the CSV file."""
        return self.transcription.columns.to_list()

    def __len__(self) -> int:
        return len(self.transcription.index)

    def __iter__(self) -> Generator[Tuple[Dict[str, str], Dict[str, str]], None, None]:
        for line_index in range(0, len(self)):
            meta_data = (
                self.transcription.iloc[line_index, : CSVTranscription.RANGE_COLUMNS[1]]
                .replace(np.nan, " ")
                .astype(str)
                .to_dict()
            )
            identity = self.transcription.iloc[line_index, CSVTranscription.RANGE_COLUMNS[1] + 1 :].dropna().to_dict()
            yield meta_data, identity

    # pylint: disable=line-too-long, too-many-branches, too-many-statements, useless-else-on-loop, no-else-return
    def format_single_line_transcription_with_entities(  # noqa: MC0001
        self,
        line_index: int,
        template: str,
        offset: int = 0,
        *,
        column_separator: str = " | ",
        empty_token: str = "∅",
        idem_token: str = "↻",
        transcribed_idem: str = "<idem>",
        empty_idem_token: str | Optional[str] = None,
        html_req: bool = True,
        tagged_empties: bool = True,
    ) -> Tuple[str, List[Entity]]:
        """
        This function contains the logic that will be used to format the transcription and entities for each line
        the requirements for each template are in the req_structs_per_template dict

        It output the transcription and entities for each line (comming a desired csv file)

        The structure of the transcription is as follows:
        "thing / other thing / empty thing next / ¤"


        :param line_index: the index (within the csv file) of the line to format
        :param template: the template of the csv file
        :param offset: the offset to begin the entities at (this is used when there are
                        multiple lines that need to be put together)
        :param column_separator: the separator to use between each row
        :param empty_token: the token to use for empty cells
        :param idem_token: the token to use for cells that are the same as the previous cell
        :transcribed_idem: how the idem token is transcribed
        :param html_req: if the transcription is to be used in html or xml
        :return: the formatted transcription and entities for each line
        """

        req_structs_per_template = {
            "category_1": {
                "requirements": [
                    "acte",
                    "sexe",
                    "leg",
                    "em",
                    "age ou delai / naissance",
                    "general",
                    "etat matrimonial",
                ],
                "ambiguous_cols": ["residence", "profession", "origine"],
            },
            "category_2": {
                "requirements": [
                    "acte",
                    "sexe",
                    "leg",
                    "em",
                    "age ou delai / naissance",
                    "general",
                    "etat matrimonial",
                ],
                "ambiguous_cols": ["residence", "profession", "origine"],
            },
            "category_3": {
                "requirements": [
                    "acte",
                    "sexe",
                    "leg",
                    "em",
                    "age ou delai / naissance",
                    "general",
                    "etat matrimonial",
                ],
                "ambiguous_cols": ["residence", "profession", "origine"],
            },
            "category_4": {
                "requirements": [
                    "acte",
                    "sexe",
                    "leg",
                    "em",
                    "age ou delai / naissance",
                    "general",
                    "origine",
                    "residence",
                    "profession",
                    "etat matrimonial",
                ],
                "ambiguous_cols": [],
            },
            "category_5": {
                "requirements": [
                    "acte",
                    "sexe",
                    "leg",
                    "em",
                    "age ou delai / naissance",
                    "general",
                    "origine",
                    "residence",
                    "profession",
                    "etat matrimonial",
                ],
                "ambiguous_cols": [],
            },
            "category_6": {
                "requirements": [
                    "acte",
                    "sexe",
                    "leg",
                    "em",
                    "age ou delai / naissance",
                    "general",
                    "etat matrimonial",
                ],
                "ambiguous_cols": ["residence", "profession", "origine"],
            },
            "category_7": {
                "requirements": [
                    "acte",
                    "sexe",
                    "leg",
                    "em",
                    "age ou delai / naissance",
                    "general",
                    "etat matrimonial",
                ],
                "ambiguous_cols": ["residence", "profession", "origine"],
            },
        }

        transcription_lines_as_dict = self._transcription_line_as_dict(line_index)
        # set all values to nan in the dict
        # transcription_lines_as_dict = {key: math.nan for key in transcription_lines_as_dict}

        # exit()
        # testing lines
        # template="category_5"
        # transcription_lines_as_dict["date jour"] = np.nan
        # transcription_lines_as_dict["date jour"] = "<idem>"
        # transcription_lines_as_dict["date mois"] = np.nan
        # transcription_lines_as_dict["date mois"] = "<idem>"
        # transcription_lines_as_dict["acte"] = np.nan
        # transcription_lines_as_dict["acte"] = "<idem>"
        # transcription_lines_as_dict["residence"] = "resid"
        # transcription_lines_as_dict["residence"] = np.nan
        # transcription_lines_as_dict["profession"] = "prof"
        # transcription_lines_as_dict["profession"] = np.nan
        # transcription_lines_as_dict["origine"] = "orgin"
        # transcription_lines_as_dict["origine"] = np.nan
        # transcription_lines_as_dict["sexe"] = np.nan
        # transcription_lines_as_dict["leg"] = np.nan
        # transcription_lines_as_dict["em"] = np.nan
        # transcription_lines_as_dict["age ou delai / naissance"] = np.nan
        # transcription_lines_as_dict["prénom enfant (ou du marié ou du décédé)"] = np.nan
        # transcription_lines_as_dict["nom du père"] = np.nan
        # transcription_lines_as_dict["prénom du père"] = np.nan
        # transcription_lines_as_dict["nom de la mère"] = np.nan
        # transcription_lines_as_dict["prénom de la mère"] = np.nan
        # transcription_lines_as_dict["acte"] = "s"

        # Variables to be used in the loop in determining the transcription and entities
        entities = []
        entity_offset = offset
        shift_value = int()
        separator = column_separator
        last_added_key = str()
        origine = transcription_lines_as_dict["origine"]
        residence = transcription_lines_as_dict["residence"]
        profession = transcription_lines_as_dict["profession"]
        transcription = str()

        if html_req:
            empty_token = html.escape(empty_token)
            idem_token = html.escape(idem_token)
            transcribed_idem = html.escape(transcribed_idem)
            column_separator = html.escape(column_separator)

        empty_idem_token = empty_token + idem_token if empty_idem_token is None else empty_idem_token

        # making the requirements and same_column_reqs variables for easier use and iterable
        requirements = req_structs_per_template[template].get("requirements", [])  # type: ignore[attr-defined]
        ambiguous_cols = req_structs_per_template[template].get("ambiguous_cols", [])  # type: ignore[attr-defined]
        # print(ambiguous_cols)

        # if there are ambiguous columns then order them (check order function to see how it works)
        ordered_ambiguous_cols = self._order_ambiguous_cols(origine, residence, profession) if ambiguous_cols else []
        skip_list = []
        for key, value in transcription_lines_as_dict.items():
            if key in skip_list:
                continue
            if isinstance(value, float) and math.isnan(value):
                value = "¤"
            value = html.escape(str(value)) if html_req else str(value)
            value = value if value != transcribed_idem else empty_idem_token
            match key, value:
                # Case 0: if the value is an idem token and it is acte, date jour, or date mois
                #  in the case of dates only one should show up
                case _ if (key == "acte" and value == empty_idem_token) or (
                    key in ("date jour", "date mois") and value == empty_idem_token
                ):
                    jour = transcription_lines_as_dict.get("date jour", "¤")
                    jour = jour if not (isinstance(jour, float) and math.isnan(jour)) else "¤"

                    mois = transcription_lines_as_dict.get("date mois", "¤")
                    mois = mois if not (isinstance(mois, float) and math.isnan(mois)) else "¤"

                    transcription = "".join([transcription, empty_idem_token + separator])
                    shift_value = len(empty_idem_token + separator)

                    entities.append(
                        Entity(
                            name=empty_idem_token.strip(),
                            type="date" if key in ("date jour", "date mois") else key.strip(),
                            offset=entity_offset,
                            length=len(empty_idem_token.strip()),
                        )
                    )
                    entity_offset += shift_value
                    if key in ("date jour", "date mois"):
                        skip_list.extend(["date jour", "date mois"])
                        last_added_key = "date mois" if mois != "¤" else "date jour" if jour != "¤" else ""
                    else:
                        last_added_key = key
                # Case 1: If the key is date jour or date mois there needs to be a . between them and they need to be added to the transcription
                #  then a entity with the name of date need to be put around both of them then they need to added to the skip list
                case _ if key in ("date jour", "date mois"):
                    added_date = ""
                    # transcription_lines_as_dict["date jour"] = np.nan
                    # print(type(transcription_lines_as_dict["date jour"]))
                    # exit()

                    jour = transcription_lines_as_dict.get("date jour", "¤")
                    jour = jour if not (isinstance(jour, float) and math.isnan(jour)) else "¤"

                    mois = transcription_lines_as_dict.get("date mois", "¤")
                    mois = mois if not (isinstance(mois, float) and math.isnan(mois)) else "¤"
                    # make a list of based on if each key is "¤" or not
                    #  like if mois is "¤" and jour is not then the list will be ["date jour"]
                    #  if both are "¤" then the list will be empty
                    #  if both are not "¤" then the list will be ["date jour", "date mois"]
                    #   if jour is "¤" and mois is not then the list will be ["date mois"]

                    if jour == "¤" and mois == "¤":
                        keys = []
                    elif jour == "¤":
                        keys = ["date mois"]
                    elif mois == "¤":
                        keys = ["date jour"]
                    else:
                        keys = ["date jour", "date mois"]
                    # check to see if either of the values are alpha values, if this happens then no . is needed and a space is needed
                    if any(x.isalpha() for x in [jour, mois]):
                        added_date = " ".join([jour, mois])
                    else:
                        for key in keys:
                            value = str(transcription_lines_as_dict.get(key, "¤"))
                            if value == "¤":
                                continue
                            # based on key is the last in the index then add a . only if it is not the last key
                            if keys.index(key) == len(keys) - 1:
                                added_date = "".join([added_date, value])
                            else:
                                added_date = "".join([added_date, value, "."])
                        else:
                            if not keys:
                                added_date = empty_token
                    transcription = "".join([transcription, added_date + separator])
                    shift_value = len(added_date + separator)

                    if not tagged_empties and not added_date == empty_token:
                        entities.append(
                            Entity(
                                name=added_date.strip(),
                                type="date",
                                offset=entity_offset,
                                length=len(added_date.strip()),
                            )
                        )
                    entity_offset += shift_value
                    skip_list.extend(["date jour", "date mois"])
                    last_added_key = "date mois" if mois != "¤" else "date jour" if jour != "¤" else ""

                # Case 2: The key is required but not required to be put with any other keys (either ambiguous or same_column)
                #  in this case things that are required will be put in the transcription with entities and separators
                case _ if key in requirements:
                    value = value if value != "¤" else empty_token
                    transcription = "".join([transcription, str(value).strip() + separator])
                    shift_value = len(str(value).strip() + separator)
                    # Case 2.0: if the value is not empty then add it to the entities and iterate no matter what
                    if value != empty_token or tagged_empties:
                        entities.append(
                            Entity(
                                name=value.strip(),
                                type=key.strip(),
                                offset=entity_offset,
                                length=len(str(value).strip()),
                            )
                        )
                    last_added_key = key
                    entity_offset += shift_value

                # Case 4: if the key is not required and is an empty cell then do nothing to the transcription
                case _ if key not in requirements and value == "¤" and not key in ambiguous_cols:  # noqa: E713
                    shift_value = 0

                # Case 5: if the key is an ambiguous column defined by the template
                #  this will be handled just like the same_column requirements
                #   one time though this case takes care of all the ambiguous columns so skip_list is used
                case _ if key in ambiguous_cols:
                    # secondary loop to get the values that are in the same column
                    for tup in ordered_ambiguous_cols:
                        # Case 5.0 if the tuple is empty then add an empty token to the transcription and a separator with no entity
                        if not tup:
                            transcription = "".join([transcription, empty_token + separator])
                            shift_value = len(empty_token + separator)
                            entity_offset += shift_value
                            last_added_key = ""
                            continue
                        # Case 5.1: loop though the tuple and add the values to the transcription without separator unless it is the last one in the tuple
                        for key in tup:
                            value = transcription_lines_as_dict.get(key, "¤")
                            # check if the key is the last one in the tuple
                            if tup.index(key) == len(tup) - 1:
                                transcription = "".join([transcription, str(value).strip() + separator])
                                shift_value = len(str(value).strip() + separator)
                                # Case 5.1.0: if the value is not empty then add it to the entities and iterate no matter what
                                if value != empty_token:
                                    entities.append(
                                        Entity(
                                            name=value.strip(),
                                            type=key.strip(),
                                            offset=entity_offset,
                                            length=len(str(value).strip()),
                                        )
                                    )
                                entity_offset += shift_value
                            else:
                                transcription += f"{value} "
                                shift_value = len(f"{value} ")
                                if value != empty_token:
                                    entities.append(
                                        Entity(
                                            name=value.strip(),
                                            type=key.strip(),
                                            offset=entity_offset,
                                            length=len(str(value).strip()),
                                        )
                                    )
                                entity_offset += shift_value
                            last_added_key = key

                    skip_list.extend(ambiguous_cols)

                # Case 6: if the key is not required we check to see if it is empty then add it if it is not empty
                case _:
                    value = value if value != "¤" else empty_token
                    # Case 6.0: if the value is not empty then add it to the transcription and entities
                    if value != empty_token:
                        transcription += f"{value} "
                        shift_value = len(f"{value} ")
                        entities.append(
                            Entity(
                                name=value.strip(),
                                type=key.strip(),
                                offset=entity_offset,
                                length=len(str(value).strip()),
                            )
                        )
                        entity_offset += shift_value

                        last_added_key = key
        # check for to see if if the last value added was of a type that is not required
        #  if it is not required then add an empty token to the end
        #   as we know the last column none of the values are required
        if last_added_key in requirements or last_added_key == "":
            transcription = "".join([transcription, empty_token])
            entity_offset += len(empty_token)

        transcription = transcription.strip()
        return transcription, entities

    def _order_ambiguous_cols(
        self, origine: Optional[str], residence: Optional[str], profession: Optional[str]
    ) -> List[Optional[Tuple[str, ...]]]:
        """
        This function will order the ambiguous columns based on the what is filled in the columns
        """
        # Typicaly the order will be [(origine), (residence)] if origine, residence are not empty and profession is empty
        # if origine, residence, and profession are not empty then the order will be [(origine,residence), (profession)]
        # if origine and profession are not empty and residence is empty then the order will be [(origine), (profession)]
        # if origine is empty and residence and profession are not empty then the order will be [(residence), (profession)]
        # if just origine is not empty then the order will be [(origine), ()]
        # if just profession is not empty then the order will be [(), (profession)]
        # if there is an act and nothing else is not empty then the order will be [(), ()]

        # Basically: origin always comes first and profession always comes last
        #  but if origin is empty and residence appears with a profession then residence comes first
        #   then the profession still comes last, if all it doesnt appear much so it is noise

        # This works though nearly every case in the entire dataset and there are few edge cases

        origine = False if isinstance(origine, float) and math.isnan(origine) else origine
        residence = False if isinstance(residence, float) and math.isnan(residence) else residence
        profession = False if isinstance(profession, float) and math.isnan(profession) else profession

        if origine and residence and profession:
            return [("origine", "residence"), ("profession",)]
        elif origine and profession and not residence:
            return [("origine",), ("profession",)]
        elif origine and not profession and residence:
            return [("origine",), ("residence",)]
        elif not origine and profession and residence:
            return [("residence",), ("profession",)]
        elif not origine and not profession and residence:
            return [tuple(), ("residence",)]
        elif origine and not profession and not residence:
            return [("origine",), tuple()]
        elif not origine and profession and not residence:
            return [tuple(), ("profession",)]
        elif not origine and not profession and not residence:
            return [tuple(), tuple()]
        else:
            return [tuple(), tuple()]

    def format_full_page_transcription_with_entities(
        self, template: str, preprocess_config: Dict[str, Any]
    ) -> Tuple[str, List[Entity]]:
        """
        Get a full page transcription of this CSV transcription.

        This uses the format_single_line_transcription_with_entities_with_structure function to get the transcription
        and entities for each line the requirements for each template are in the req_structs_per_template dict
        within the format_single_line_transcription_with_entities_with_structure function

        :param template: the template of the csv file that will be
                            used in the format_single_line_transcription_with_entities_with_structure function
        :return: the formatted transcription and entities for each line
        """

        formatted_transcription = ""
        entity_objects = []
        current_character_offset_for_entity_position = 0
        # Iteration over all the lines of the transcription
        for index_transcription in range(0, len(self)):
            # use the same process as in format_single_line_transcription_with_entities_with_structure
            # to get the transcription and entities for each line

            transcription, entities = self.format_single_line_transcription_with_entities(
                index_transcription,
                template,
                current_character_offset_for_entity_position,
                **(preprocess_config or {}),
            )
            formatted_transcription += transcription + "\n"
            entity_objects += entities
            # every time a new line is made, the offsets for the lines after it need to be shifted to
            # account for the new line with the added \n
            current_character_offset_for_entity_position += len(transcription) + 1

        return formatted_transcription, entity_objects

    @staticmethod
    def _format_transcription_from_dict_to_string(transcription_as_dict: Dict[str, Any]) -> str:
        return " ".join(
            [str(int(value)) if isinstance(value, float) else str(value) for value in transcription_as_dict.values()]
        )

    def _transcription_line_as_dict_without_nan_values(self, index_line: int) -> Dict[str, str]:
        """
        Get the line of a transcription as a dictionary. Values of empty columns are removed, keeping only the
        columns with a real value.
        """
        return self.transcription.iloc[index_line].replace(r"^\s*$", np.nan, regex=True).dropna().to_dict()

    def _transcription_line_as_dict(self, index_line: int) -> Dict[str, str]:
        """
        Get a dictionary of columns and values for a given line of the CSV transcription.

        :param index_line: the index of the line to get
        :return: the dictionary of columns and values for the given line
        """
        return self.transcription.iloc[index_line].replace(r"^\s*$", np.nan, regex=True).drop("remarque").to_dict()

    @property
    def as_pandas_dataframe(self) -> DataFrame:
        return self.transcription.copy()


class CSVTranscriptionsDataLoader:
    """
    Load CSV Transcriptions for the whole dataset.
    """

    def __init__(self, path_to_csv_transcriptions: Path):
        self._path_to_csv_transcriptions = path_to_csv_transcriptions
        self._transcription_files = list(Path(self._path_to_csv_transcriptions).glob("**/*.csv"))

    def get_random(self) -> CSVTranscription:
        """Get a random CSVTranscription from the whole dataset."""
        return CSVTranscription(random.choice(self._transcription_files))
