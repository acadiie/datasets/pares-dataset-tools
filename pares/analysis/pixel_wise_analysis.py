#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT


from pathlib import Path
from typing import Any, Dict, List

import numpy as np
import pandas as pd
import tqdm

from pares.common.arkindex import ArkIndexAPI
from pares.stats.full_dataset_stats import (
    conglom_mask_and_pix_stats,
    find_box_pixel_loss,
)
from pares.stats.plotting import (
    save_boxplot_of_single_numpy_array,
    save_image_of_numpy_array,
)
from pares.stats.utils import count_outliers, flatten_list


def create_statistics_csv_with_images(
    api: ArkIndexAPI,
    folder_uuid: str,
    splits: Dict[str, Dict[str, str]],
    entity_names: List[str],
    output_stats_folder: Path,
) -> None:
    """
    Create a csv file with statistics about the dataset and images of the conglomerate masks.
    This function is a wrapper around the functions that do the actual work. (for the most part)

    :param api: The ArkIndexAPI object
    :param folder_uuid: The uuid of the folder containing the dataset
    :param splits: A dictionary containing the uuids of the splits
    :param entity_names: A list of the entity names
    :param output_stats_folder: The folder where the output will be saved
    :return: None
    """
    pages: Dict[str, List[Dict[str, Any]]] = {}
    print("Loading pages")

    for split in tqdm.tqdm(splits["ttv"].items(), desc="ttv_splits"):
        pages[split[0]] = api.list_element_children_with_specific_class(folder_uuid, split[1])

    page_lines: Dict[str, List[List[Dict[str, Any]]]] = {}

    for split in tqdm.tqdm(pages.keys(), desc="split", leave=True, colour="green"):  # type: ignore
        page_lines[split] = []  # type: ignore
        for page in tqdm.tqdm(pages[split], leave=False, desc=split, colour="blue"):  # type: ignore
            lines = api.list_element_children_with_specific_class(page["id"])
            page_lines[split].append(lines)  # type: ignore

    print("Starting full page analysis")
    df = pd.DataFrame()
    df = full_page_analysis(df, pages, page_lines, entity_names, output_stats_folder)
    df.T.to_csv(output_stats_folder / "full_dataset_stats.csv")

    print("Starting split analysis")
    df = split_analysis(df, pages, page_lines, entity_names, output_stats_folder)
    df.T.to_csv(output_stats_folder / "full_dataset_stats.csv")

    print("Starting category analysis")

    pages = {}

    for category in tqdm.tqdm(splits["categories"].items(), desc="category_splits"):
        pages[category[0]] = api.list_element_children_with_specific_class(folder_uuid, category[1])

    page_lines = {}

    for category in tqdm.tqdm(pages.keys(), desc="category_line_loading", leave=True):  # type: ignore
        page_lines[category] = []  # type: ignore
        for page in tqdm.tqdm(pages[category], leave=False, desc=category, colour="green"):  # type: ignore
            lines = api.list_element_children_with_specific_class(page["id"])
            page_lines[category].append(lines)  # type: ignore

    df = category_analysis(df, pages, page_lines, entity_names, output_stats_folder)
    df.index = [
        "pages",
        "min",
        "max",
        "mean",
        "median",
        "std",
        "var",
        "outliers",
        "box_pixel_loss",
        "lines",
        "average_pixels",
        "average_coverage",
    ]

    df.T.to_csv(output_stats_folder / "full_dataset_stats.csv")


def full_page_analysis(
    df: pd.DataFrame,
    pages: Dict[str, List[Dict[str, Any]]],
    page_lines: Dict[str, List[List[Dict[str, Any]]]],
    entity_names: List[str],
    output_stats_folder: Path,
) -> pd.DataFrame:
    """
    This function does the analysis for the full dataset. It gets the statistics for the full dataset.
    It also gets the statistics for each entity type in the dataset. It also gets the statistics for each split.
    It saves the conglomerate mask, the boxplot, and the npy array for each split and entity type.
    It outputs a dataframe with all of the statistics to saved as a csv file.

    :param df: The dataframe to add the statistics to
    :param pages: A dictionary containing the pages of the dataset
    :param page_lines: A dictionary containing the lines of the dataset
    :param entity_names: A list of the entity names
    :param output_stats_folder: The folder where the output will be saved
    :return: The dataframe with the statistics added
    """
    # getting a list of entity counts per split per page into a dictionary
    page_lengths_all = {spit: [len(page) for page in page_lines[spit]] for spit in page_lines.keys()}
    # put all of the values together to get a full look at the dataset
    all_lengths = flatten_list(list(page_lengths_all.values()))

    # getting all the pages and lines together rather than in their respective splits
    full_page_pages = [page for split in pages.values() for page in split]
    full_page_lines = [line for split in page_lines.values() for line in split]

    cong_mask, average_pixels, average_coverage = conglom_mask_and_pix_stats(full_page_pages, full_page_lines)
    save_image_of_numpy_array(cong_mask, output_stats_folder, "full_dataset_conglomerate_mask", "nipy_spectral")
    np.save(output_stats_folder / ("full_dataset_conglomerate_mask.npy"), cong_mask)
    save_boxplot_of_single_numpy_array(
        all_lengths, output_stats_folder, "full_dataset_boxplot", xlabel="Number of Entities", figsize=(10, 2)
    )

    df["full dataset"] = [
        len(all_lengths),
        np.min(all_lengths),
        np.max(all_lengths),
        np.mean(all_lengths),
        np.median(all_lengths),
        np.std(all_lengths),
        np.var(all_lengths),
        count_outliers(all_lengths),
        find_box_pixel_loss(flatten_list(flatten_list(list(page_lines.values())))),
        len(flatten_list(list(page_lines.values()))),
        average_pixels,
        average_coverage,
    ]
    for ent in tqdm.tqdm(entity_names, desc="entity", leave=False, colour="blue"):
        # filter the lines by the page entity type
        filtered_lines = [[line for line in page if line["type"] == ent] for page in full_page_lines]
        filtered_lines_lengths = [len(page) for page in filtered_lines]

        cong_mask, average_pixels, average_coverage = conglom_mask_and_pix_stats(full_page_pages, filtered_lines)
        # add a new column to the dataframe called "full_dataset_"+ split name
        df["full_dataset_" + ent] = [
            len(full_page_pages),
            np.min(filtered_lines_lengths),
            np.max(filtered_lines_lengths),
            np.mean(filtered_lines_lengths),
            np.median(filtered_lines_lengths),
            np.std(filtered_lines_lengths),
            np.var(filtered_lines_lengths),
            count_outliers(filtered_lines_lengths),
            find_box_pixel_loss(flatten_list(filtered_lines)),
            len([val for val in flatten_list(filtered_lines) if val["type"] == ent]),
            average_pixels,
            average_coverage,
        ]

        # saving the image of the conglomerate mask, npy array, and boxplot
        save_image_of_numpy_array(
            cong_mask, output_stats_folder, ("full_dataset_" + ent + "_conglomerate_mask"), "nipy_spectral"
        )
        np.save(output_stats_folder / ("full_dataset_" + ent + "_conglomerate_mask.npy"), cong_mask)
        save_boxplot_of_single_numpy_array(
            filtered_lines_lengths,
            output_stats_folder,
            ("full_dataset_" + ent + "_boxplot"),
            xlabel=f"Number of {ent} entities",
            figsize=(10, 2),
        )
    return df


def split_analysis(
    df: pd.DataFrame,
    pages: Dict[str, List[Dict[str, Any]]],
    page_lines: Dict[str, List[List[Dict[str, Any]]]],
    entity_names: List[str],
    output_stats_folder: Path,
) -> pd.DataFrame:
    """
    This function does the analysis for the splits. It gets the statistics for the splits.
    It also gets the statistics for each entity type in the split. It saves the conglomerate mask,
    the boxplot, and the npy array for each split and entity type.
    It outputs a dataframe with all of the statistics to saved as a csv file.

    :param df: The dataframe to add the statistics to
    :param pages: A dictionary containing the pages of the dataset
    :param page_lines: A dictionary containing the lines of the dataset
    :param entity_names: A list of the entity names
    :param output_stats_folder: The folder where the output will be saved
    :return: The dataframe with the statistics added
    """
    # getting a list of entity counts per split per page into a dictionary
    page_lengths_all = {spit: [len(page) for page in page_lines[spit]] for spit in page_lines.keys()}

    for split in tqdm.tqdm(page_lengths_all.keys(), desc="split", leave=True, colour="green"):
        flattened_lines = flatten_list(list(page_lines[split]))
        cong_mask, average_pixels, average_coverage = conglom_mask_and_pix_stats(pages[split], page_lines[split])

        df[split + "_all"] = [
            len(page_lengths_all[split]),
            np.min(page_lengths_all[split]),
            np.max(page_lengths_all[split]),
            np.mean(page_lengths_all[split]),
            np.median(page_lengths_all[split]),
            np.std(page_lengths_all[split]),
            np.var(page_lengths_all[split]),
            count_outliers(page_lengths_all[split]),
            find_box_pixel_loss(flattened_lines),
            len(flattened_lines),
            average_pixels,
            average_coverage,
        ]

        # saving the image of the conglomerate mask, npy array, and boxplot
        save_image_of_numpy_array(cong_mask, output_stats_folder, (split + "_conglomerate_mask"), "nipy_spectral")
        np.save(output_stats_folder / (split + "_conglomerate_mask.npy"), cong_mask)
        save_boxplot_of_single_numpy_array(
            page_lengths_all[split],
            output_stats_folder,
            (split + "_boxplot"),
            xlabel="Number of Entities",
            figsize=(10, 2),
        )
        for ent in tqdm.tqdm(entity_names, desc="entity", leave=False, colour="blue"):
            # filter the lines by the page entity type
            filtered_lines = [[line for line in page if line["type"] == ent] for page in page_lines[split]]
            filtered_lines_lengths = [len(page) for page in filtered_lines]

            # get the pixel box loss for the entity type
            flattened_lines = flatten_list(list(filtered_lines))

            cong_mask, average_pixels, average_coverage = conglom_mask_and_pix_stats(pages[split], filtered_lines)
            df[split + "_" + ent] = [
                len(pages[split]),
                np.min(filtered_lines_lengths),
                np.max(filtered_lines_lengths),
                np.mean(filtered_lines_lengths),
                np.median(filtered_lines_lengths),
                np.std(filtered_lines_lengths),
                np.var(filtered_lines_lengths),
                count_outliers(filtered_lines_lengths),
                find_box_pixel_loss(flattened_lines),
                len(flattened_lines),
                average_pixels,
                average_coverage,
            ]

            # saving the image of the conglomerate mask, npy array, and boxplot
            save_image_of_numpy_array(
                cong_mask, output_stats_folder, (split + "_" + ent + "_conglomerate_mask"), "nipy_spectral"
            )
            np.save(output_stats_folder / (split + "_" + ent + "_conglomerate_mask.npy"), cong_mask)
            save_boxplot_of_single_numpy_array(
                filtered_lines_lengths,
                output_stats_folder,
                (split + "_" + ent + "_boxplot"),
                xlabel=f"Number of {ent} entities",
                figsize=(10, 2),
            )
    return df


def category_analysis(
    df: pd.DataFrame,
    pages: Dict[str, List[Dict[str, Any]]],
    page_lines: Dict[str, List[List[Dict[str, Any]]]],
    entity_names: List[str],
    output_stats_folder: Path,
) -> pd.DataFrame:
    """
    This function does the analysis for the categories. It gets the statistics for the categories.
    It also gets the statistics for each entity type in the category. It saves the conglomerate mask,
    the boxplot, and the npy array for each category and entity type.
    It outputs a dataframe with all of the statistics to saved as a csv file.

    :param df: The dataframe to add the statistics to
    :param pages: A dictionary containing the pages of the dataset
    :param page_lines: A dictionary containing the lines of the dataset
    :param entity_names: A list of the entity names
    :param output_stats_folder: The folder where the output will be saved
    :return: The dataframe with the statistics added
    """
    # getting a list of entity counts per category per page into a dictionary
    page_lengths_all = {spit: [len(page) for page in page_lines[spit]] for spit in page_lines.keys()}

    for category in tqdm.tqdm(page_lengths_all.keys(), desc="category", leave=True, colour="green"):
        flattened_lines = flatten_list(list(page_lines[category]))
        cong_mask, average_pixels, average_coverage = conglom_mask_and_pix_stats(pages[category], page_lines[category])

        df[category + "_all"] = [
            len(page_lengths_all[category]),
            np.min(page_lengths_all[category]),
            np.max(page_lengths_all[category]),
            np.mean(page_lengths_all[category]),
            np.median(page_lengths_all[category]),
            np.std(page_lengths_all[category]),
            np.var(page_lengths_all[category]),
            count_outliers(page_lengths_all[category]),
            find_box_pixel_loss(flattened_lines),
            len(flattened_lines),
            average_pixels,
            average_coverage,
        ]

        # saving the image of the conglomerate mask, npy array, and boxplot
        save_image_of_numpy_array(cong_mask, output_stats_folder, (category + "_conglomerate_mask"), "nipy_spectral")
        np.save(output_stats_folder / (category + "_conglomerate_mask.npy"), cong_mask)
        save_boxplot_of_single_numpy_array(
            page_lengths_all[category],
            output_stats_folder,
            (category + "_boxplot"),
            xlabel="Number of Entities",
            figsize=(10, 2),
        )

        for ent in tqdm.tqdm(entity_names, desc="entity", leave=False, colour="blue"):
            # filter the lines by the page entity type
            filtered_lines = [[line for line in page if line["type"] == ent] for page in page_lines[category]]
            filtered_lines_lengths = [len(page) for page in filtered_lines]

            flattened_lines = flatten_list(list(filtered_lines))

            cong_mask, average_pixels, average_coverage = conglom_mask_and_pix_stats(pages[category], filtered_lines)
            df[category + "_" + ent] = [
                len(pages[category]),
                np.min(filtered_lines_lengths),
                np.max(filtered_lines_lengths),
                np.mean(filtered_lines_lengths),
                np.median(filtered_lines_lengths),
                np.std(filtered_lines_lengths),
                np.var(filtered_lines_lengths),
                count_outliers(filtered_lines_lengths),
                find_box_pixel_loss(flattened_lines),
                len(flattened_lines),
                average_pixels,
                average_coverage,
            ]

            # saving the image of the conglomerate mask, npy array, and boxplot
            save_image_of_numpy_array(
                cong_mask, output_stats_folder, (category + "_" + ent + "_conglomerate_mask"), "nipy_spectral"
            )
            np.save(output_stats_folder / (category + "_" + ent + "_conglomerate_mask.npy"), cong_mask)
            save_boxplot_of_single_numpy_array(
                filtered_lines_lengths,
                output_stats_folder,
                (category + "_" + ent + "_boxplot"),
                xlabel=f"Number of {ent} entities",
                figsize=(10, 2),
            )

    return df
