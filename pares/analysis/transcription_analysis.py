#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

import json
import re
import statistics
from collections import Counter
from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
import pandas as pd
from nltk.tokenize import RegexpTokenizer, word_tokenize
from pandas import DataFrame
from tqdm import tqdm

from pares.common.arkindex import ArkIndexAPI
from pares.stats.plotting import save_boxplot_of_single_numpy_array
from pares.stats.utils import count_outliers


def readCsvFile(path: Path) -> DataFrame:
    """
    Reads a csv file and returns a dataframe

    :param path: the path to the csv file
    :return: the dataframe of the csv file
    """
    df = pd.read_csv(path, sep=";", dtype=object, header=0, encoding="utf-8")
    return df


def preprocessDataframe(df: DataFrame) -> DataFrame:
    """
    Preprocesses a dataframe dropping unnecessary columns and rows

    :param df: the dataframe to preprocess
    :return: the preprocessed dataframe
    """
    columns_to_drop = ["NOM de la paroisse", "DATE Année", "Remarque"]
    df = (
        df.drop([x.casefold() for x in columns_to_drop], axis=1)
        .replace(r"^\s*$", np.nan, regex=True)
        .dropna(axis=1, how="all")
        .dropna(axis=0, how="all")
    )
    return df


def get_spilt_to_csv(
    api: ArkIndexAPI, folder_uuid: str, split_dict: Dict[str, str], trans_path: Path
) -> Dict[str, List[Path]]:
    """
    Gets a dictionary that maps a split to a list of csv files (in path form) that belong to that split

    :param api: the ArkIndexAPI object
    :param folder_uuid: the uuid of the folder to get the csv files from
    :param split_dict: the dictionary that maps a split to a class uuid
    :param trans_path: the path to the transcriptions
    :return: a dictionary that maps a split to a list of csv files (in path form) that belong to that split
    """
    split_to_pages: Dict[str, List[str]] = {}
    for key, value in split_dict.items():
        resp = api.list_element_children_with_specific_class(folder_uuid, value)
        names = [x["name"].split("__")[1] for x in resp]
        split_to_pages[key] = names
    csvs = [Path(i) for i in Path(trans_path).glob("**/*.csv")]

    split_to_csvs: Dict[str, List[Path]] = {key: [] for key in split_to_pages}

    for key, val in split_to_pages.items():
        split_to_csvs[key].extend([x for x in csvs if x.stem.casefold() in val])
    return split_to_csvs


def get_page_and_split_dataframes(
    split_to_csvs: Dict[str, List[Path]]
) -> Tuple[Dict[str, DataFrame], Dict[str, DataFrame]]:
    """
    Creates two dictionaries, one that contains dataframes per split will all the information from that split
    and one that contains dataframes per page with all the information from that page

    :param split_to_csvs: the dictionary that maps a split to a list of csv
                             files (in path form) that belong to that split
    :return: a tuple of the two dictionaries, one that contains dataframes per split will all
            the information from that split and one that contains dataframes per page with all
            the information from that page
    """
    split_dfs = {}
    page_dfs = {}
    for split in split_to_csvs:
        split_dfs[split] = pd.DataFrame()
        for file in split_to_csvs[split]:
            new_df = preprocessDataframe(pd.read_csv(file, sep=",", header=0, dtype=object))
            # put this dataframe into the main dataframe
            split_dfs[split] = pd.concat(
                [split_dfs[split], new_df], axis=0, ignore_index=True, join="outer", sort=False
            )
            # also put it into the page_dfs
            page_dfs[file.stem] = new_df
    return split_dfs, page_dfs


def count_breakdown(counter: Dict) -> Dict:
    """
    Breaks down the dictionary forms of Counters into their
    respective categories (letters, words, numbers, special characters,
    mixed values)
    :param counter: the counter to break down, this could be a Counter from the
                     collections library turned into a dictionary
    :return: a dictionary that maps a category to a dictionary of the counts for each character in that category
                the categories are letters, words, numbers, special characters, and mixed
    """
    # make dictionaries that will hold the counts for each character in their respective category
    letters = {}
    words = {}
    numbers = {}
    special_characters = {}
    mixed = {}
    pattern_for_special_character = r"^[^a-zA-Z0-9']*'*[^a-zA-Z0-9']*$"
    for key, value in counter.items():
        # use a match case to determine which category the character belongs to
        match key:
            case x if len(x) == 1 and x.isalpha():
                letters[key] = value
            case x if len(x) > 1 and x.isalpha():
                words[key] = value
            case x if x.isdigit():
                numbers[key] = value
            case x if re.match(pattern_for_special_character, x):
                special_characters[key] = value
            case _:
                mixed[key] = value
    # sort the dictionaries in descending order
    letters = dict(sorted(letters.items(), key=lambda x: x[1], reverse=True))
    words = dict(sorted(words.items(), key=lambda x: x[1], reverse=True))
    numbers = dict(sorted(numbers.items(), key=lambda x: x[1], reverse=True))
    special_characters = dict(sorted(special_characters.items(), key=lambda x: x[1], reverse=True))
    mixed = dict(sorted(mixed.items(), key=lambda x: x[1], reverse=True))
    # sort the returned dictionary in alphabetical order by key
    return dict(
        sorted(
            {
                "letters": letters,
                "words": words,
                "numbers": numbers,
                "special_characters": special_characters,
                "mixed": mixed,
            }.items(),
            key=lambda x: x[0],
        )
    )


def get_char_counts_from_splits(split_dfs: Dict[str, DataFrame], output_folder: Path) -> None:
    """
    Gets the character counts from the splits and saves them as a json file

    :param split_dfs: the dictionary that maps a split to a dataframe with all the information from that split
    :param output_folder: the path to the folder where the json files will be saved
                             containing the character count information
    :return: None
    """
    all_rows = []
    all_stats = {}
    for split in tqdm(split_dfs, desc="Split", leave=False, colour="blue"):
        rows = []
        for _, row in split_dfs[split].iterrows():
            translated_row = "".join([str(x) for x in row if str(x) != "nan"])

            all_rows.append(translated_row)
            rows.append(translated_row)
        all_stats[split] = dict(Counter(list("".join(rows))))
    all_stats["all_rows"] = dict(Counter(list("".join(all_rows))))
    # organize the the dictionaries in descending order for each key
    all_stats = {
        key: dict(sorted(value.items(), key=lambda x: x[1], reverse=True)) for key, value in all_stats.items()
    }
    # organize the keys in the dictionary alphabetically
    all_stats = dict(sorted(all_stats.items(), key=lambda x: x[0]))

    broken_down = {key: count_breakdown(value) for key, value in all_stats.items()}

    broken_down = dict(sorted(broken_down.items(), key=lambda x: x[0]))
    with open(output_folder / "character_counts_breakdown.json", "w", encoding="utf-8") as f:
        f.write(json.dumps(broken_down, ensure_ascii=False, indent=4))

    with open(output_folder / "character_counts.json", "w", encoding="utf-8") as f:
        f.write(json.dumps(all_stats, ensure_ascii=False, indent=4))


def word_tokenizer_french(text: str) -> List[str]:
    """
    Tokenizes a string into words using the french word tokenizer from the nltk library
    with an added regex tokenizer that will split the string into words and special characters
    to ensure words like "l'homme" are tokenized like ["l", "'", "homme"] instead of ["l'homme"]

    :param text: the string to tokenize
    :return: the list of tokens
    """
    custom_tokenizer = RegexpTokenizer(r"\w+|[^\w\s]")
    return [word_tokenize(token, language="french")[0] for token in custom_tokenizer.tokenize(text)]


def get_word_counts_from_splits(split_dfs: Dict[str, DataFrame], output_folder: Path) -> None:
    """
    Gets the word counts from the splits and saves them as a json file
    while also saving the word counts broken down into their respective categories

    :param split_dfs: the dictionary that maps a split to a dataframe with all the information from that split
    :param output_folder: the path to the folder where the json files will be saved containing
                             the word countinformation
    :return: None
    """
    all_rows = []
    all_stats = {}
    for split in tqdm(split_dfs, desc="Split", leave=False, colour="blue"):
        rows = []
        for _, row in split_dfs[split].iterrows():
            translated_row = " ".join([str(x) for x in row if str(x) != "nan"])
            all_rows.append(translated_row)
            rows.append(translated_row)
        all_stats[split] = dict(Counter(word_tokenizer_french(" ".join(rows))))

    all_stats["all_rows"] = dict(Counter(word_tokenizer_french(" ".join(all_rows))))

    all_stats = {
        key: dict(sorted(value.items(), key=lambda x: x[1], reverse=True)) for key, value in all_stats.items()
    }
    all_stats = dict(sorted(all_stats.items(), key=lambda x: x[0]))
    broken_down = {key: count_breakdown(value) for key, value in all_stats.items()}
    # sort the keys in the dictionary alphabetically
    broken_down = dict(sorted(broken_down.items(), key=lambda x: x[0]))
    with open(output_folder / "word_counts_breakdown.json", "w", encoding="utf-8") as f:
        f.write(json.dumps(broken_down, ensure_ascii=False, indent=4))
    with open(output_folder / "word_counts.json", "w", encoding="utf-8") as f:
        f.write(json.dumps(all_stats, ensure_ascii=False, indent=4))


def character_analysis_page_wise(page_dfs: Dict[str, DataFrame], output_folder: Path) -> None:
    """
    Does a character analysis on the pages and lines then saves the stats as a csv file
    with the box and whisker plots of the number of characters per page and per line

    :param page_dfs: the dictionary that maps a page to a dataframe with all the information from that page
    :param output_folder: the path to the folder where the csv files and box and whisker plots will be
                             saved containing the character count information
    :return: None
    """
    all_rows = []
    num_chars_per_page = []
    for page in tqdm(page_dfs, desc="Page", leave=False, colour="blue"):
        rows = []
        for _, row in page_dfs[page].iterrows():
            translated_row = "".join([str(x) for x in row if str(x) != "nan"])
            rows.append(translated_row)
            all_rows.append(translated_row)
        num_chars_per_page.append(len("".join(rows)))

    per_line = [len(x) for x in all_rows]

    df = DataFrame(
        columns=["mean", "median", "mode", "std", "variance", "total_character_count", "outliers", "occurances"],
        index=["characters_per_page", "characters_per_line"],
    )
    df.loc["characters_per_page"] = [
        np.mean(num_chars_per_page),
        np.median(num_chars_per_page),
        statistics.mode(num_chars_per_page),
        np.std(num_chars_per_page),
        np.var(num_chars_per_page),
        np.sum(num_chars_per_page),
        count_outliers(num_chars_per_page),
        len(num_chars_per_page),
    ]
    df.loc["characters_per_line"] = [
        np.mean(per_line),
        np.median(per_line),
        statistics.mode(per_line),
        np.std(per_line),
        np.var(per_line),
        np.sum(per_line),
        count_outliers(per_line),
        len(per_line),
    ]
    df.to_csv(output_folder / "character_analysis.csv")

    save_boxplot_of_single_numpy_array(
        np.array(num_chars_per_page),
        output_folder,
        "characters_per_page",
        "Number of characters",
        (10, 2),
    )

    save_boxplot_of_single_numpy_array(
        np.array(per_line),
        output_folder,
        "characters_per_line",
        "Number of characters",
        (10, 2),
    )


def word_analysis_page_wise(page_dfs: Dict[str, DataFrame], output_folder: Path) -> None:
    """
    Does a word analysis on the pages and lines then saves the stats as a csv file
    with the box and whisker plots of the number of words per page and per line
    :param page_dfs: the dictionary that maps a page to a dataframe with all the information from that page
    :param output_folder: the path to the folder where the csv files and box and whisker plots
                            will be saved containing the word count information
    :return: None
    """
    all_rows = []
    num_words_per_page = []
    for page in tqdm(page_dfs, desc="Page", leave=False, colour="blue"):
        rows = []
        for _, row in page_dfs[page].iterrows():
            translated_row = " ".join([str(x) for x in row if str(x) != "nan"])
            rows.append(translated_row)
            all_rows.append(translated_row)
        # find the number of words per page by using the word tokenizer
        num_words_per_page.append(len(word_tokenizer_french(" ".join(rows))))

    per_line = [len(word_tokenizer_french(x)) for x in all_rows]
    # make a dataframe that will hold the stats and save it as a csv file
    df = DataFrame(
        columns=["mean", "median", "mode", "std", "variance", "total_word_count", "outliers", "occurances"],
        index=["words_per_page", "words_per_line"],
    )
    df.loc["words_per_page"] = [
        np.mean(num_words_per_page),
        np.median(num_words_per_page),
        statistics.mode(num_words_per_page),
        np.std(num_words_per_page),
        np.var(num_words_per_page),
        sum(num_words_per_page),
        count_outliers(num_words_per_page),
        len(num_words_per_page),
    ]
    df.loc["words_per_line"] = [
        np.mean(per_line),
        np.median(per_line),
        statistics.mode(per_line),
        np.std(per_line),
        np.var(per_line),
        np.sum(per_line),
        count_outliers(per_line),
        len(per_line),
    ]
    df.to_csv(output_folder / "word_analysis.csv")

    # save the box and whisker plot of the number of words per page
    save_boxplot_of_single_numpy_array(
        np.array(num_words_per_page),
        output_folder,
        "words_per_page",
        "Number of words",
        (10, 2),
    )

    # save the box and whisker plot of the number of words per line
    save_boxplot_of_single_numpy_array(np.array(per_line), output_folder, "words_per_line", "Number of words", (10, 2))


def run_transcription_analysis(
    api: ArkIndexAPI,
    folder_uuid: str,
    split_dict: Dict[str, str],
    trans_path: Path,
    output_folder: Path,
) -> None:
    """
    Runs the transcription analysis on the transcriptions in the folder with the given uuid
    and saves the results in the given output folder

    :param api: the ArkIndexAPI object
    :param folder_uuid: the uuid of the folder to get the csv files from
    :param split_dict: the dictionary that maps a split to a class uuid
    :param trans_path: the path to the transcriptions
    :param output_folder: the path to the folder where the results will be saved
    :return: None
    """

    split_to_csvs = get_spilt_to_csv(api, folder_uuid, split_dict, trans_path)
    split_dfs, page_dfs = get_page_and_split_dataframes(split_to_csvs)

    page_wise_analysis = (word_analysis_page_wise, character_analysis_page_wise)

    for analysis in tqdm(page_wise_analysis, desc="Page wise analysis", colour="green"):
        analysis(page_dfs, output_folder)

    split_wise_analysis = (get_char_counts_from_splits, get_word_counts_from_splits)

    for analysis in tqdm(split_wise_analysis, desc="Split wise analysis", colour="green"):  # type: ignore
        analysis(split_dfs, output_folder)
