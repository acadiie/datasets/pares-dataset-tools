#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

import argparse
from pathlib import Path

from pares.analysis.pixel_wise_analysis import create_statistics_csv_with_images
from pares.analysis.show_counter_from_dict import save_counter_image_from_query
from pares.analysis.transcription_analysis import run_transcription_analysis
from pares.common import read_config_file_json
from pares.common.arkindex import ArkIndexAPI


def main():
    parser = argparse.ArgumentParser(description="Output statistics that describe the PARES dataset.")
    subparsers = parser.add_subparsers(dest="actions")
    subparsers.add_parser(
        "pix_stats", help="Output statistics and images about the annotations, pixel-wise, of the images."
    )
    subparsers.add_parser(
        "trans_stats",
        help="Output statistics about the transcriptions, character and word-wise, of the images",
    )
    subparsers.add_parser(
        "show_counter",
        help="Saves an image of the counter at the path given by the user (can only be run after trans_stats)",
    )
    parser.add_argument(
        "--config-file",
        help="Path to the JSON file containing the configuration to export the files",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--output-folder",
        help="Path to the folder where to output the stats",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--path_to_json",
        help="Path to the json file containing the counter dict (only can be used after trans_stats has be run)",
        type=str,
        required=False,
    )

    args = parser.parse_args()

    Path(args.output_folder).mkdir(parents=True, exist_ok=True)

    config = read_config_file_json(args.config_file)

    match args.actions:
        case "pix_stats":
            api = ArkIndexAPI.create_from_environment()
            splits = {"categories": config["categories_uuids"], "ttv": config["splits_uuid"]}
            create_statistics_csv_with_images(
                api,
                config["arkindex_image_folder_id"],
                splits,
                config["entity_names"],
                Path(args.output_folder),
            )
        case "trans_stats":
            api = ArkIndexAPI.create_from_environment()
            run_transcription_analysis(
                api,
                config["arkindex_image_folder_id"],
                config["splits_uuid"],
                Path(config["transcriptions_path"]),
                Path(args.output_folder),
            )
        case "show_counter":
            if args.path_to_json is None:
                raise ValueError("path_to_json must be present")
            save_counter_image_from_query(args.path_to_json, args.output_folder)
        case _:
            raise ValueError(f"Unknown action {args.actions}")
