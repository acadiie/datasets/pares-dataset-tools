#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT


import json
import re
from pathlib import Path
from pprint import pprint
from typing import Dict, List

from matplotlib import pyplot as plt

from pares.stats.plotting import make_chart_from_counter_with_prompts


def save_counter_image_from_query(path_to_json: str, output_folder: str) -> None:
    """
    This fuction will save an image of the counter at the path given by the user.
    It is wraps most of the logic of the show_counter command.

    :param path_to_json: Path to the json file containing the counter dict
    :param output_folder: Path to the folder where to output the stats
    :return: None
    """
    # show the data down  the path_to_json
    with open(path_to_json, encoding="utf-8") as json_file:
        data = json.load(json_file)
    # pretty print it with small indentation
    pprint(data, indent=0, compact=True)
    query = get_query_from_user()
    counter = get_count_dict_from_path(path_to_json, query)

    fig, _ = make_chart_from_counter_with_prompts(counter)

    save_chart(fig, output_folder)


def get_query_from_user() -> List[str]:
    """
    This function will ask the user to put in the path to the desired counter.

    :return: a list of keys to get to find the counter in the json file
    """
    # ask the user to put in the path to the desired counter
    json_query = input("Please put in the path to the desired counter (key1,key2,...): ")
    query = json_query.split(",")
    query = [key.strip() for key in query]
    return query


def get_count_dict_from_path(path_to_json: str, json_query: List[str]) -> Dict[str, int]:
    """
    This function will get the counter from the path based on user input.

    :path_to_json: Path to the json file containing the counter dict
    :json_query: a list of keys to get to find the counter in the json file
    :return: the counter
    """

    with open(path_to_json, encoding="utf-8") as json_file:
        data = json.load(json_file)
    # the query is in the form key1,key2,key3 (keys until it hits the counter)
    for key in json_query:
        data = data[key]
    # error message to make sure the data is a counter (a Dict[str,int])
    if not isinstance(data, dict) or not all(isinstance(k, str) and isinstance(v, int) for k, v in data.items()):
        raise TypeError(
            """
            The data is not a type that can be considered a counter.
            A counter is a dict with str keys and int values or Dict[str,int].
            """
        )

    return data


def save_chart(fig: plt.Figure, output_folder: str) -> None:
    """
    This function will save the chart to the output_folder and prompt the user to put in a name for the output file.

    :param fig: the figure to save
    :param output_folder: the folder to save the figure in
    :return: None
    """

    # prompt the user to put in a name for the output file
    while True:
        output_name = input("Please put in a name for the output file: ")
        if re.match(r"^[\w\s,-\.]+$", output_name):
            break
        print("Invalid filename. Please use only letters, numbers, underscores, spaces, commas, and hyphens.")
    if Path(output_name).suffix == "":
        output_name += ".png"

    # save the chart to the output_folder and make it tight
    fig.savefig(Path(output_folder, output_name))
