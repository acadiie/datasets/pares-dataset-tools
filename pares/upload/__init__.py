#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT
import argparse
import logging
from pathlib import Path

from pares.common import read_config_file_json
from pares.common.arkindex import ArkIndexAPI
from pares.upload.categories import create_classification_on_image
from pares.upload.entities import (
    create_entities_on_arkindex_corpus,
    delete_all_entities_within_a_corpus,
    delete_all_entity_types_within_a_corpus,
)
from pares.upload.transcriptions import (
    check_single_line_transcription_style,
    destroy_all_single_line_transcriptions_within_a_folder,
    destroy_full_page_transcriptions_within_a_folder,
    upload_full_page_transcriptions,
    upload_single_line_transcriptions_row_level,
)

logging.basicConfig(level=logging.ERROR)


def main():
    parser = argparse.ArgumentParser(description="Create, delete transcriptions, entities or categories on ArkIndex.")

    parser.add_argument(
        "--config-file",
        help="Path to the JSON file containing the configuration to export the files",
        type=str,
        required=True,
    )

    subparsers = parser.add_subparsers(dest="actions")

    subparser_transcriptions = subparsers.add_parser("transcriptions", help="Create, delete transcriptions.")
    subparser_transcriptions.add_argument(
        "--destroy-full-page",
        action="store_true",
        help="Delete all full page transcriptions within a folder from specified worker run UUID",
    )
    subparser_transcriptions.add_argument(
        "--destroy-single-line",
        action="store_true",
        help="""Delete all single line transcriptions within a folder
from specified worker run UUID - has Multithreading""",
    )
    subparser_transcriptions.add_argument(
        "--upload-full-page",
        action="store_true",
        help="Upload full page transcriptions with structure with specified worker run UUID",
    )

    subparser_transcriptions.add_argument(
        "--upload-single-line",
        action="store_true",
        help="""Upload single line transcriptions with structure for
row level analysis with specified worker run UUID - has Multithreading""",
    )

    subparser_transcriptions.add_argument(
        "--check-single-line-transcription-style",
        action="store_true",
        help="""Check the style of the transcriptions base on the pre-processing config file and it
will show spaces as · with entities in the text like entity_type[entity_value]""",
    )

    subparser_transcriptions.add_argument(
        "--check-full-page-transcription-style",
        action="store_true",
        help="""Check the style of the transcriptions base on the pre-processing config file and it
will show spaces as · with entities in the text like entity_type[entity_value]""",
    )

    subparser_transcriptions.add_argument(
        "--pre-process-config",
        help="Path to the JSON file the options for pre-processing the PARES dataset",
        type=str,
        required=False,
    )

    subparser_transcriptions.add_argument(
        "--fast",
        action="store_true",
        help="""If used will check if Multithreading is available and use it if possible
          *WARNING* Could possibly cause errors""",
    )

    subparser_entities = subparsers.add_parser(
        "entities",
        help="Create, delete entities.",
    )
    subparser_entities.add_argument("--create-entities", action="store_true")
    subparser_entities.add_argument("--delete-entities-and-types", action="store_true")
    subparser_entities.add_argument("--delete-entities", action="store_true")

    subparser_entities.add_argument(
        "--fast",
        action="store_true",
        help="""If used will check if Multithreading is available and use it if possible
          *WARNING* Could possibly cause errors""",
    )

    subparser_categories = subparsers.add_parser("categories", help="Create, delete categories.")
    subparser_categories.add_argument("--create-categories", action="store_true")
    subparser_categories.add_argument(
        "--categories",
        required=True,
        type=str,
    )

    args = parser.parse_args()
    config = read_config_file_json(args.config_file)
    api = ArkIndexAPI.create_from_environment()

    arkindex_folder_uuid = config["arkindex_image_folder_id"]
    transcriptions_path = Path(config["transcriptions_path"])
    corpus_uuid = config["corpus_uuid"]

    fast = args.fast

    match args.actions:
        case "transcriptions":
            # Single line transcriptions
            if args.destroy_single_line:
                destroy_all_single_line_transcriptions_within_a_folder(api, arkindex_folder_uuid, fast)

            # Full page transcriptions
            if args.destroy_full_page:
                destroy_full_page_transcriptions_within_a_folder(api, arkindex_folder_uuid, fast)

            if args.upload_full_page:
                preprocess_config = read_config_file_json(args.pre_process_config) if args.pre_process_config else None
                upload_full_page_transcriptions(
                    api,
                    transcriptions_path,
                    arkindex_folder_uuid,
                    corpus_uuid,
                    preprocess_config,
                    fast,
                )
            if args.upload_single_line:
                preprocess_config = read_config_file_json(args.pre_process_config) if args.pre_process_config else None
                upload_single_line_transcriptions_row_level(
                    api, transcriptions_path, arkindex_folder_uuid, corpus_uuid, preprocess_config, fast
                )
            if args.check_single_line_transcription_style:
                preprocess_config = read_config_file_json(args.pre_process_config) if args.pre_process_config else None
                check_single_line_transcription_style(
                    api, transcriptions_path, arkindex_folder_uuid, preprocess_config
                )

        case "entities":
            if args.create_entities:
                create_entities_on_arkindex_corpus(api, transcriptions_path, corpus_uuid)
            if args.delete_entities_and_types:
                delete_all_entity_types_within_a_corpus(api, corpus_uuid, fast)
            if args.delete_entities:
                delete_all_entities_within_a_corpus(api, corpus_uuid, fast)

        case "categories":
            if args.create_categories and args.categories:
                categories = read_config_file_json(args.categories)
                create_classification_on_image(api, arkindex_folder_uuid, categories, config.get("categories_uuids"))


if __name__ == "__main__":
    main()
