#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

"""
CRUD operations for ‘Entity Type’ on ArkIndex.
"""

import random
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path

from tqdm import tqdm

from pares.common.arkindex import ArkIndexAPI
from pares.common.transcription import CSVTranscriptionsDataLoader


def generate_random_rgb_color() -> str:
    return f"{random.randint(0,255):x}{random.randint(0,255):x}{random.randint(0,255):x}"


def create_entities_on_arkindex_corpus(api: ArkIndexAPI, path_to_transcriptions: Path, corpus_uuid: str) -> None:
    """
    Create ‘Entity Type’ on Arkindex based on a single, randomly chosen transcription.
    """
    random_transcription = CSVTranscriptionsDataLoader(path_to_transcriptions).get_random()
    annotated_element_colors = {
        annotated_element: generate_random_rgb_color() for annotated_element in random_transcription.annotated_elements
    }
    for annotated_element_name, annotated_element_color in annotated_element_colors.items():
        api.create_entity_type_on_corpus(corpus_uuid, annotated_element_name, annotated_element_color)


def delete_all_entity_types_within_a_corpus(api: ArkIndexAPI, corpus_uuid: str, threaded: bool) -> None:
    """
    Delete all ‘Entity Type’ elements within an ArkIndex corpus.
    """
    delete_all_entities_within_a_corpus(api, corpus_uuid, threaded)
    if not threaded:
        entity_types = api.list_corpus_entity_types(corpus_uuid)
        for entity in tqdm(entity_types, desc="Destroying entity types"):
            api.delete_entity_type(entity.get("id"))
    else:
        entity_types = api.list_corpus_entity_types(corpus_uuid)
        with ThreadPoolExecutor() as executor:
            for entity in tqdm(entity_types, desc="Destroying entity types"):
                executor.submit(api.delete_entity_type, entity.get("id"))


def delete_all_entities_within_a_corpus(api: ArkIndexAPI, corpus_uuid: str, threaded: bool) -> None:
    """
    Delete all ‘Entity Type’ elements within an ArkIndex corpus.
    """
    entities = api.list_corpus_entities(corpus_uuid)
    if not threaded:
        for entity in tqdm(entities, desc="Destroying entities"):
            api.delete_entity(entity.get("id"))
    else:
        with ThreadPoolExecutor() as executor:
            for entity in tqdm(entities, desc="Destroying entities"):
                executor.submit(api.delete_entity, entity.get("id"))


def delete_all_entities_within_a_list(api: ArkIndexAPI, entities: list, threaded: bool) -> None:
    """
    Delete all ‘Entity Type’ elements within an ArkIndex corpus.
    """
    if not threaded:
        for entity in entities:
            api.delete_entity(entity.get("id"))
    else:
        with ThreadPoolExecutor() as executor:
            for entity in entities:
                executor.submit(api.delete_entity, entity.get("entity").get("id"))
