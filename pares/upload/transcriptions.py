#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

"""
CRUD operations for ‘Transcription’ on Arkindex. Page or text line transcriptions.
"""
import html
import logging
import random
import re
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
from typing import Any, Dict, Optional

import pandas as pd
from tqdm import tqdm

from pares.common.arkindex import (
    APIException,
    ArkIndexAPI,
    map_page_uuids_with_csv_files,
)
from pares.common.transcription import CSVTranscription
from pares.upload.entities import delete_all_entities_within_a_list


# region Destruction functions (need fixed to work with worker queries)
def destroy_full_page_transcriptions_within_a_folder(
    api: ArkIndexAPI, arkindex_image_folder_id: str, threaded: bool
) -> None:
    """
    Destroy all full page transcriptions within a folder.
    *note: this will use the worker run id specified in the .env file*

    :param api: ArkIndexAPI
    :param arkindex_image_folder_id: UUID of the folder containing the images
    :return: None
    """
    transcriptions = api.list_page_transcriptions_in_a_folder(folder_uuid=arkindex_image_folder_id)
    if not threaded:
        for trans in tqdm(transcriptions, desc="Deleting transcriptions"):
            entities = api.list_transcription_entities_of_transcription(trans.get("id"))
            api.delete_transcription(trans["id"])
            delete_all_entities_within_a_list(api, entities, threaded)
    if threaded:
        with ThreadPoolExecutor() as executor:
            for trans in tqdm(transcriptions, desc="Deleting transcriptions"):
                entities = api.list_transcription_entities_of_transcription(trans.get("id"))
                executor.submit(api.delete_transcription, trans.get("id"))
                delete_all_entities_within_a_list(api, entities, threaded)


def destroy_all_single_line_transcriptions_within_a_folder(
    api: ArkIndexAPI, arkindex_image_folder_id: str, threaded: bool
) -> None:
    """
    Destroy all single line transcriptions within a folder.
    *note: this will use the worker id specified in the .env file*

    :param api: ArkIndexAPI
    :param arkindex_image_folder_id: UUID of the folder containing the images
    :param threaded: Boolean to indicate whether to use multithreading or not
    :return: None
    """
    pages = api.list_element_children(arkindex_image_folder_id)
    if not threaded:
        for page in tqdm(pages):
            text_lines = api.list_page_text_lines(page_uuid=page.get("id"))
            for text_line in tqdm(text_lines, leave=False, colour="blue"):
                transcriptions = api.list_transcriptions_of_text_line(text_line.get("id"))
                if not transcriptions:
                    continue
                for transcription in transcriptions:
                    api.delete_transcription(transcription.get("id"))

    if threaded:

        def delete_transcriptions(page):
            text_lines = api.list_page_text_lines(page_uuid=page.get("id"))
            for text_line in text_lines:
                transcriptions = api.list_transcriptions_of_text_line(text_line.get("id"))

                for transcription in transcriptions:
                    entities = api.list_transcription_entities_of_transcription(transcription.get("id"))
                    api.delete_transcription(transcription.get("id"))
                    delete_all_entities_within_a_list(api, entities, threaded)

        with ThreadPoolExecutor() as executor:
            list(tqdm(executor.map(delete_transcriptions, pages), total=len(pages), desc="Deleting transcriptions"))


# endregion


# region Check functions
def check_single_line_transcription_style(
    api: ArkIndexAPI,
    path_to_image_cvs_folder: Path,
    arkindex_image_folder_uuid: str,
    preprocessing_config: Optional[dict],
) -> None:
    map_page_uuid_csv_file = map_page_uuids_with_csv_files(api, path_to_image_cvs_folder, arkindex_image_folder_uuid)
    # get a random key value pair from the map
    page_uuid, transcription_csv_file = random.choice(list(map_page_uuid_csv_file.items()))
    # get the classifications of the page
    classes = [x["ml_class"]["name"] for x in api.get_element_json_from_element_uuid(page_uuid)["classifications"]]
    template = [x for x in classes if re.match(r"category_\d", x)][0]
    # get how many lines are in the csv file
    nbr_lines = len(pd.read_csv(transcription_csv_file))
    # get a number between 0 and the number of lines in the csv file
    line_index = random.randint(0, nbr_lines - 1)
    # show what the transcription looks like
    csv_transcription = CSVTranscription(transcription_csv_file)
    (
        transcription,
        entities,
    ) = csv_transcription.format_single_line_transcription_with_entities(
        line_index, template, **(preprocessing_config or {})
    )
    print(f"Category: {template}")
    print(f"Transcription: {transcription}")
    print()
    print(_transcription_with_entities(transcription, entities))


def check_full_page_transcription_style(
    api: ArkIndexAPI,
    path_to_image_cvs_folder: Path,
    arkindex_image_folder_uuid: str,
    preprocessing_config: Optional[Dict[str, Any]],
) -> None:
    map_page_uuid_csv_file = map_page_uuids_with_csv_files(api, path_to_image_cvs_folder, arkindex_image_folder_uuid)
    # get a random key value pair from the map
    page_uuid, transcription_csv_file = random.choice(list(map_page_uuid_csv_file.items()))
    # get the classifications of the page
    classes = [x["ml_class"]["name"] for x in api.get_element_json_from_element_uuid(page_uuid)["classifications"]]
    template = [x for x in classes if re.match(r"category_\d", x)][0]
    # show what the transcription looks like
    csv_transcription = CSVTranscription(transcription_csv_file)

    (
        transcription,
        entities,
    ) = csv_transcription.format_full_page_transcription_with_entities(
        template, preprocessing_config  # type: ignore
    )
    print(f"Category: {template}")
    print(f"Transcription: {transcription}")
    print()
    print(_transcription_with_entities(transcription, entities))


# endregion


# region Row level single line transcription functions
# pylint: disable=too-many-locals, consider-using-sys-exit, unreachable, too-many-arguments
def upload_single_line_transcriptions_row_level(
    api: ArkIndexAPI,
    path_to_image_cvs_folder: Path,
    arkindex_image_folder_uuid: str,
    corpus_uuid: str,
    preprocessing_config: Optional[dict],
    threaded: bool,
) -> None:
    """
    Upload single line transcriptions of all the pages contained in a specific folder of a corpus on ArkIndex.
    It includes uploading both raw transcription and the transcription with entities.
    This one creates transcriptions with structural information included.
    Also this one is row level, meaning the transcriptions are better suited for row level analysis.

    :param api: ArkIndexAPI
    :param path_to_image_cvs_folder: Path to the folder containing the CSV files
    :param arkindex_image_folder_uuid: UUID of the folder containing the images
    :param corpus_uuid: UUID of the corpus
    :param threaded: Boolean to indicate whether to use multithreading or not
    :return: None
    """
    # Gaining a map of page uuids and csv files so that we can iterate over them
    map_page_uuid_csv_file = map_page_uuids_with_csv_files(api, path_to_image_cvs_folder, arkindex_image_folder_uuid)

    # slower version without threads
    if not threaded:
        for _, (page_uuid, transcription_csv_file) in tqdm(enumerate(list(map_page_uuid_csv_file.items()))):
            _process_and_upload_structured_lines_row_level(
                api, page_uuid, transcription_csv_file, corpus_uuid, preprocessing_config
            )

    if threaded:
        with ThreadPoolExecutor() as executor:
            futures = []
            for page_uuid, transcription_csv_file in map_page_uuid_csv_file.items():
                future = executor.submit(
                    _process_and_upload_structured_lines_row_level,
                    api,
                    page_uuid,
                    transcription_csv_file,
                    corpus_uuid,
                    preprocessing_config,
                )
                futures.append(future)

            # Wait for all task_process_and_upload_structured_lines_row_levels to complete
            for future in tqdm(futures, desc="Processing pages"):
                future.result()


def _transcription_with_entities(transcription: str, entities: list) -> str:
    # Sort entities in reverse order by offset
    entities.sort(key=lambda entity: entity.offset, reverse=True)
    test = transcription
    for ent in entities:
        test = test[: ent.offset] + ent.type + "[" + test[ent.offset :]
        end_index = ent.offset + len(ent.type) + ent.length + 1
        test = test[:end_index] + "]" + test[end_index:]

    return html.unescape(test).replace(" ", "·")


def _process_and_upload_structured_lines_row_level(
    api: ArkIndexAPI,
    page_uuid: str,
    transcription_csv_file: Path,
    corpus_uuid: str,
    preprocessing_config: Optional[dict] = None,
) -> None:
    classes = [x["ml_class"]["name"] for x in api.get_element_json_from_element_uuid(page_uuid)["classifications"]]
    template = [x for x in classes if re.match(r"category_\d", x)][0]
    # Load all lines ordered based on their vertical center (top to bottom)
    lines = api.list_page_text_lines(page_uuid)
    csv_transcription = CSVTranscription(transcription_csv_file)

    for line_index, line_of_text in enumerate(lines):
        try:
            (
                transcription,
                entities,
            ) = csv_transcription.format_single_line_transcription_with_entities(
                line_index, template, **(preprocessing_config or {})
            )

            # reverse the order of the entities based on their offset
            entities = sorted(entities, key=lambda x: x.offset, reverse=True)
            # print()
            # print(_transcription_with_entities(transcription, entities))
            # # continue
            # exit(0)
            # column_separator = " | "
            # columns_len = len(transcription.split(column_separator))
            #             if template in ("category_5", "category_4"):
            #                 if columns_len != 12:
            #                     raise ValueError(
            # f"""template - {template} has {columns_len} columns instead of 12 on line {line_index}
            # {_transcription_with_entities(transcription, entities)}\n""")
            #             else:
            #                 if columns_len != 11:
            #                     raise ValueError(
            # f"""template - {template} has {columns_len} columns instead of 11 on line {line_index}
            # {_transcription_with_entities(transcription, entities)}\n""")

            api.create_line_transcription_with_entities(corpus_uuid, line_of_text.get("id"), transcription, entities)

        except APIException as api_error:
            logging.error(
                "Error with %s (line%d: %s) (%s)", transcription_csv_file, line_index, line_of_text, api_error
            )


# endregion


# region Full page transcription functions


# pylint: disable=too-many-arguments
def upload_full_page_transcriptions(
    api: ArkIndexAPI,
    path_to_image_cvs_folder: Path,
    arkindex_image_folder_uuid: str,
    corpus_uuid: str,
    preprocess_config: Optional[dict],
    threaded: bool,
) -> None:
    """
    Upload full page transcriptions of all the pages contained in a specific folder of a corpus on ArkIndex.
    It includes uploading both raw transcription and the transcription with entities.
    This one creates transcriptions with structural information included.

    :param api: ArkIndexAPI
    :param path_to_image_cvs_folder: Path to the folder containing the CSV files
    :param arkindex_image_folder_uuid: UUID of the folder containing the images
    :param corpus_uuid: UUID of the corpus
    :return: None
    """
    map_page_uuid_csv_file = map_page_uuids_with_csv_files(api, path_to_image_cvs_folder, arkindex_image_folder_uuid)

    if not threaded:
        for page_uuid, transcription_csv_file in tqdm(list(map_page_uuid_csv_file.items())):
            _process_and_upload_full_page_transcriptions(
                api, page_uuid, transcription_csv_file, corpus_uuid, preprocess_config
            )
            # exit()
    else:
        # exit()
        with ThreadPoolExecutor() as executor:
            futures = []
            for page_uuid, transcription_csv_file in map_page_uuid_csv_file.items():
                future = executor.submit(
                    _process_and_upload_full_page_transcriptions,
                    api,
                    page_uuid,
                    transcription_csv_file,
                    corpus_uuid,
                    preprocess_config,
                )
                futures.append(future)

            # Wait for all task_process_and_upload_structured_lines_row_levels to complete
            for future in tqdm(futures, desc="Processing pages"):
                future.result()


def _process_and_upload_full_page_transcriptions(
    api: ArkIndexAPI,
    page_uuid: str,
    transcription_csv_file: Path,
    corpus_uuid: str,
    preprocessing_config: Optional[dict] = None,
) -> None:
    classes = [x["ml_class"]["name"] for x in api.get_element_json_from_element_uuid(page_uuid)["classifications"]]
    template = [x for x in classes if re.match(r"category_\d", x)][0]
    csv_transcription = CSVTranscription(transcription_csv_file)
    (
        full_page_transcription,
        transcription_entities,
    ) = csv_transcription.format_full_page_transcription_with_entities(
        template, preprocessing_config  # type: ignore
    )
    # print(full_page_transcription)

    # # reverse the order of the entities based on their offset
    # entities = sorted(transcription_entities, key=lambda x: x.offset, reverse=True)
    # test = full_page_transcription
    # # add the entities to the transcription
    # for ent in entities:
    #     test = test[: ent.offset] + ent.type + "[" + test[ent.offset :]
    #     end_index = ent.offset + len(ent.type) + ent.length + 1
    #     test = test[:end_index] + "]" + test[end_index:]

    api.create_page_transcription_with_entities(
        corpus_uuid, page_uuid, full_page_transcription, transcription_entities
    )


# endregion
