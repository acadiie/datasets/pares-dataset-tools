#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

"""
CRUD operations for ‘Category’ on ArkIndex
"""

from typing import Dict

from tqdm import tqdm

from pares.common.arkindex import ArkIndexAPI


def create_classification_on_image(api: ArkIndexAPI, folder_uuid: str, categories: Dict, categories_uuids: Dict):
    map_image_name_uuid = api.map_images_names_and_uuids_from_within_a_folder(folder_uuid)
    for category_name, category in categories.items():
        for image in tqdm(category.get("images")):
            image_uuid = map_image_name_uuid[image]
            api.create_classification_on_image(image_uuid, categories_uuids[category_name])
