#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

import argparse
from pathlib import Path
from pprint import pprint

import dotenv

from pares.checks.intersections import (
    check_for_intersections_in_pages_within_a_folder_on_arkindex,
)
from pares.checks.transcriptions import (
    images_statistics_with_difference_in_count_compared_to_transcriptions,
)
from pares.common import read_config_file_json
from pares.common.arkindex import ArkIndexAPI


def main():
    parser = argparse.ArgumentParser(description="Perform checks on the PARES dataset stored on an ArkIndex instance.")
    subparsers = parser.add_subparsers(dest="actions")

    subparsers.add_parser("intersections", help="Ensure that there is no overlap between any polygon.")
    subparsers.add_parser(
        "transcriptions",
        help="Ensure that the number of transcription matches the number of annotation of type ’text_line’.",
    )
    subparsers.add_parser("check-env", help="Check the environment variables.")
    parser.add_argument(
        "--config-file",
        help="Path to the JSON file containing the configuration to export the files",
        type=str,
        required=True,
    )
    args = parser.parse_args()
    config = read_config_file_json(args.config_file)
    api = ArkIndexAPI.create_from_environment()
    match args.actions:
        case "intersections":
            pages_with_intersections = check_for_intersections_in_pages_within_a_folder_on_arkindex(
                api,
                config.get("arkindex_image_folder_id"),
            )
            for page_with_intersections in pages_with_intersections:
                print(page_with_intersections)
        case "transcriptions":
            lines_statistics = images_statistics_with_difference_in_count_compared_to_transcriptions(
                api,
                Path(config.get("transcriptions_path")),
                config.get("arkindex_image_folder_id"),
            )
            for file_with_non_matching_number_of_lines in lines_statistics:
                print(file_with_non_matching_number_of_lines)
        case "check-env":
            # load the dotenv file and print the environment variables
            dotenv.load_dotenv()
            pprint(dict(dotenv.dotenv_values()))


if __name__ == "__main__":
    main()
