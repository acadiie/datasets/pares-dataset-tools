#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

"""
Module to check whether the number of transcriptions in CSV files match the number of text lines on Arkindex.
"""

from pathlib import Path
from typing import Set, Tuple

from tqdm import tqdm

from pares.common.arkindex import ArkIndexAPI, map_page_uuids_with_csv_files
from pares.common.transcription import CSVTranscription


def images_statistics_with_difference_in_count_compared_to_transcriptions(
    api: ArkIndexAPI, csv_path: Path, folder_uuid_on_arkindex: str
) -> Set[Tuple[str, str, int]]:
    """
    Get the statistics of text lines for each annotated image. The difference in count is
    shown only if there is a difference between the number of lines found in the image
    compared to the number of lines given in the transcriptions.
    """
    map_uuid_csv_filenames = map_page_uuids_with_csv_files(api, csv_path, folder_uuid_on_arkindex)
    images_with_difference_in_count = set()
    for arkindex_file_uuid, csv_filename in tqdm(map_uuid_csv_filenames.items(), desc="Checking images"):
        difference_in_lines = difference_in_count_of_text_lines_between_arkindex_file_and_csv_transcription(
            api, arkindex_file_uuid, csv_filename
        )
        if difference_in_lines > 0:
            images_with_difference_in_count.add((arkindex_file_uuid, csv_filename.name, difference_in_lines))
    return images_with_difference_in_count


def difference_in_count_of_text_lines_between_arkindex_file_and_csv_transcription(
    api: ArkIndexAPI, page_uuid: str, csv_filename: Path
) -> int:
    """
    Get the difference in count of the text lines of an image, compared to its known transcription.
    """
    lines = api.list_page_text_lines(page_uuid)
    return abs(len(lines) - len(CSVTranscription(csv_filename)))
