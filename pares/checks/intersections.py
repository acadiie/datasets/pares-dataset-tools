#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

"""
Module to check whether there are intersections between polygons of annotations on ArkIndex.
"""

from typing import List, Set, Tuple

import matplotlib.pyplot as plt
from shapely.geometry import Polygon
from tqdm import tqdm

from pares.common.arkindex import ArkIndexAPI


def has_intersect_or_touches(polygons: List[Polygon], image_name: str | None = None) -> bool:
    """
    Check if the polygons has intersections or touches.

    :param polygons: the list of Polygon to check
    :param image_name: when filled, in case of an intersection, they are shown using matplotlib.

    :return: True if at least two polygons of the input list have touches or intersections.
    """
    intersections = []
    for polygon in polygons:
        if not polygon.is_valid:
            intersections.append((polygon, polygon))

    for first_polygon_index, first_polygon in enumerate(polygons):
        for second_polygon_index, second_polygon in enumerate(polygons):
            if (
                first_polygon.intersects(second_polygon) or first_polygon.touches(second_polygon)
            ) and first_polygon_index != second_polygon_index:
                intersections.append((first_polygon, second_polygon))

    has_intersection = len(intersections) > 0
    if has_intersection and image_name is not None:
        display_intersections(image_name, intersections, polygons)

    return has_intersection


def display_intersections(image_name: str, intersections: List, polygons: List[Polygon]) -> None:
    """
    Display, using matplotlib, all the polygons and the intersections between the polygons.
    """
    _, ax = plt.subplots()
    ax.set_title(f"Intersection between polygons on: {image_name}")
    for polygon in polygons:
        x, y = polygon.exterior.xy
        plt.plot(x, y, color="grey")
    for intersection in intersections:
        if intersection[0] == intersection[1]:
            x, y = intersection[0].exterior.xy
            plt.fill(x, y, color="red")
            continue
        x, y = intersection[0].exterior.xy
        plt.plot(x, y, color="blue")
        x, y = intersection[1].exterior.xy
        plt.plot(x, y, color="blue")
        intersection = intersection[0].intersection(intersection[1])
        if intersection.geom_type == "MultiPolygon":
            for geom in intersection.geoms:
                x, y = geom.exterior.xy
                plt.fill(x, y, color="red")
        if intersection.geom_type == "Point":
            x, y = intersection.xy
            plt.plot(x, y, color="red", marker="o")
        if intersection.geom_type == "Polygon":
            x, y = intersection.exterior.xy
            plt.fill(x, y, color="red")

    ax.invert_yaxis()
    ax.set_aspect("equal")
    plt.show()


def check_for_intersections_in_pages_within_a_folder_on_arkindex(
    api: ArkIndexAPI, folder_uuid: str
) -> Set[Tuple[str, str]]:
    """
    Check whether there are intersections between the polygons of the Page contained in the given folder.

    :return: the list, as tuples (page uuid, page name) of pages where an intersection between polygons exists.
    """
    pages_with_intersections = set()
    pages = api.list_element_children(folder_uuid)
    for page in tqdm(pages, desc="Checking intersections in pages"):
        page_uuid, page_name = page.get("id"), page.get("name")
        lines = api.list_element_children(page_uuid)
        if has_intersect_or_touches([Polygon(line["zone"]["polygon"]) for line in lines], page_name):
            pages_with_intersections.add((page_uuid, page_name))
    return pages_with_intersections
