#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

import argparse
from pathlib import Path

from pares.common import read_config_file_json
from pares.common.arkindex import ArkIndexAPI
from pares.export.annotations_from_arkindex import (
    export_pares_annotations_from_arkindex_specified,
    export_pares_annotations_from_arkindex_teklia,
)


def main():
    parser = argparse.ArgumentParser("Export annotations from ArkIndex (Teklia software)")

    parser.add_argument("--teklia-format", help="Export annotations in Teklia format", action="store_true")

    parser.add_argument("--other-format", help="Export annotations in an XML format", action="store_true")

    parser.add_argument(
        "--output-directory",
        "-o",
        help="Where to store the exported files",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--config-file",
        help="Path to the JSON file containing the configuration to export the files",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--fast",
        help="Whether or not to use the multitheaded version of the export",
        action="store_true",
    )
    parser.add_argument(
        "--tagged",
        help="Whether or not to use the tagged version of the export",
        action="store_true",
    )

    parser.add_argument(
        "--pre-process-config",
        help="Path to the JSON file the options for pre-processing the PARES dataset",
        type=str,
        required=False,
    )
    parser.add_argument(
        "--download-images",
        help="Download the images from the ArkIndex instance",
        action="store_true",
    )

    parser.add_argument(
        "--format",
        help="Format of the annotations to export",
        type=str,
        choices=["page", "transcribus", "coco"],
        required=False,
    )

    parser.add_argument(
        "--txt-line-only",
        help="Export only the table lines",
        action="store_true",
    )

    args = parser.parse_args()
    config = read_config_file_json(args.config_file)
    arkindex_folder_uuid = config["arkindex_image_folder_id"]
    transcriptions_path = Path(config["transcriptions_path"])
    fast = args.fast
    tagged = args.tagged
    download_images = args.download_images

    api = ArkIndexAPI.create_from_environment()

    if args.teklia_format:
        export_pares_annotations_from_arkindex_teklia(api, Path(args.output_directory), config)
    if args.other_format:
        preprocess_config = read_config_file_json(args.pre_process_config) if args.pre_process_config else None

        export_pares_annotations_from_arkindex_specified(
            api=api,
            output_directory=Path(args.output_directory),
            path_to_image_cvs_folder=transcriptions_path,
            arkindex_image_folder_uuid=arkindex_folder_uuid,
            tagged=tagged,
            preprocess_config=preprocess_config,
            threaded=fast,
            download_images=download_images,
            format_=args.format,
            txt_line_only=args.txt_line_only,
        )


if __name__ == "__main__":
    main()
