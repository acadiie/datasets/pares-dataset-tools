#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

"""
Export annotations for PARES from Arkindex.
"""


import glob
import html
import json
import os
import re
import shutil
import subprocess
import sys
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timezone
from pathlib import Path
from typing import Any, Dict, List, Optional

import requests
from dotenv import load_dotenv
from requests.exceptions import ChunkedEncodingError
from shapely.geometry import Polygon
from tenacity import retry, retry_if_exception_type, stop_after_attempt, wait_fixed
from tqdm import tqdm
from unidecode import unidecode

from pares.common.arkindex import ArkIndexAPI, Entity, map_page_uuids_with_csv_files
from pares.common.transcription import CSVTranscription


# region Export annotations from Arkindex in Teklia format
def build_run_extraction_command(output_directory: Path, config: Dict[str, Any]) -> List[str]:
    """
    Build the ‘run-extraction’ command coming from the arkindex export tool.
    :param output_directory: directory where to output labels, images and labels_json
    :param config: the config file, loaded as a dictionary
    :return: the elements of the command, in a list
    """
    command = [
        "run-extraction",
        "--corpus",
        f"{config.get('corpus_uuid')}",
        "--parents-types",
        "folder",
        "--output-directory",
        f"{output_directory}",
        "--split-process",
        "--eps 0",
        "--subsets",
        f"\"{config.get('arkindex_image_folder_name')}\"",
    ]

    if "image_size" in config:
        command.extend(["--img-size", str(config.get("image_size"))])

    if "annotator_uuid" in config:
        command.extend(["--worker-version-id", f"{config.get('annotator_uuid')}"])

    classes_cmd = ["--classes"]
    colors_cmd = ["--colors"]
    if "classes" not in config.keys():
        raise ValueError(
            "You need to add the ‘classes’ items in your configuration file with the colors that match each class, "
            "in order to export the annotations. "
            "See https://gitlab.teklia.com/dla/arkindex_document_layout_training_label_normalization for more "
            "information."
        )

    for class_name, class_color in config.get("classes", {}).items():
        classes_cmd.append(class_name)
        colors_cmd.append(class_color)

    command.extend(classes_cmd)
    command.extend(colors_cmd)

    return command


def run_extraction_command(command: List[str]) -> None:
    """
    Run the command on the host system.
    :param command: the elements of the command, in a list
    """
    load_dotenv()
    with subprocess.Popen(" ".join(command), shell=True, env=os.environ.copy()) as run_extraction_process:
        run_extraction_process.wait()


def move_downloaded_files_to_skip_the_subset_name(output_directory: Path, folder_name: str) -> None:
    """
    Downloaded files are stored, because of how 'run-extraction' command runs, in a sub folder
    that is called after the subset we selected. We move all those files from the subset to its
    direct parent folder.
    :param output_directory: the directory where the files are exported to
    :param folder_name: the name of the subset.
    """
    for sub_directory_name in ["images", "labels", "labels_json"]:
        origin = output_directory / sub_directory_name / folder_name
        destination = output_directory / sub_directory_name
        move_directory_content_to_other_place(origin, destination)
        os.rmdir(origin)


def move_directory_content_to_other_place(origin: Path, destination: Path) -> None:
    """
    Move all the files from the 'origin' directory to 'destination'.
    """
    for src_path in origin.iterdir():
        dst_path = str(os.path.join(destination, src_path.name))
        shutil.move(src_path, dst_path)


def rename_downloaded_file_with_the_original_corpus_name(api: "ArkIndexAPI", output_directory: Path) -> None:
    """
    Rename the downloaded image files from their UUID to the image names that were first uploaded on Arkindex.
    :param api: api to access Arkindex
    :param output_directory: where the elements where downloaded to.
    """
    arkindex_exported_files = glob.glob(f"{output_directory}/images/*.png")
    for file in tqdm(arkindex_exported_files, desc="Renaming exported files"):
        directory, _, filename = file.split("/")
        uuid = filename[:-4]
        new_name = api.get_element_name_from_element_uuid(uuid)
        for file_extension, directories in [
            ("json", ["labels_json", "metadata"]),
            ("png", ["images", "labels"]),
        ]:
            for directory in directories:
                os.rename(
                    output_directory / directory / f"{uuid}.{file_extension}",
                    output_directory / directory / f"{new_name}.{file_extension}",
                )


def export_metadata_for_image_files(api: ArkIndexAPI, corpus_uuid: str, output_directory: Path) -> None:
    """
    Export the metadata information from Arkindex and stores the output in a ‘metadata’ folder.

    :param api: api to access Arkindex
    :param corpus_uuid: the UUID of the corpus
    :param output_directory: where the elements where downloaded to.
    :return: None
    """
    output_metadata_directory = output_directory / "metadata"
    output_metadata_directory.mkdir(parents=True, exist_ok=True)

    for page in tqdm(api.list_element_children(corpus_uuid)):
        element = api.get_element_json_from_element_uuid(page.get("id"))
        with open(output_metadata_directory / f"{element.get('id')}.json", mode="w", encoding="utf-8") as o_file:
            json.dump(element, o_file, indent=2)


def export_pares_annotations_from_arkindex_teklia(
    api: ArkIndexAPI, output_directory: Path, config: Dict[str, Any]
) -> None:
    """
    I am not 100% sure what all happens here as I did not write this.
    I beleive Teklia created their own annotation/transcription format.
    This format does not work with any software outside of the arkindex platform.

    :param api: api to access Arkindex
    :param output_directory: where the elements where downloaded to.
    :param config: the config file, loaded as a dictionary
    :return: None
    """
    folder_uuid = config["arkindex_image_folder_id"]
    folder_name = config["arkindex_image_folder_name"]

    run_extraction_command(build_run_extraction_command(output_directory, config))
    move_downloaded_files_to_skip_the_subset_name(output_directory, folder_name)
    export_metadata_for_image_files(api, folder_uuid, output_directory)
    rename_downloaded_file_with_the_original_corpus_name(api, output_directory)


# endregion


# region Export annotations from Arkindex into an xml format
# pylint: disable=line-too-long, too-many-branches, too-many-statements, useless-else-on-loop, no-else-return, too-many-locals, too-many-arguments
def export_pares_annotations_from_arkindex_specified(
    api: ArkIndexAPI,
    output_directory: Path,
    path_to_image_cvs_folder: Path,
    arkindex_image_folder_uuid: str,
    tagged: bool,
    preprocess_config: Optional[dict],
    threaded: bool,
    download_images: bool = False,
    format_: str = "page",
    txt_line_only: bool = False,
) -> None:
    """
    Export annotations from Arkindex in some different xml formats.
    """

    # Gaining a map of page uuids and csv files so that we can iterate over them
    map_page_uuid_csv_file = map_page_uuids_with_csv_files(api, path_to_image_cvs_folder, arkindex_image_folder_uuid)

    match format_:
        case "page":
            chosen_function = _process_structured_lines_in_page  # type: ignore
        case "transcribus":
            chosen_function = _process_structured_lines_in_transcribus  # type: ignore
        case "coco":
            chosen_function = _process_structured_lines_in_coco  # type: ignore
        case _:
            raise ValueError(f"Unknown format: {format_}")

    if threaded:
        # print the number of threads that will be used to process the pages
        print(f"Using {os.cpu_count()} threads to process the pages")
        with ThreadPoolExecutor() as executor:
            futures = []
            progress_bar = tqdm(total=len(map_page_uuid_csv_file.items()), desc="Processing pages")
            for i, (page_uuid, transcription_csv_file) in enumerate(list(map_page_uuid_csv_file.items()), start=1):
                future = executor.submit(
                    chosen_function,
                    api,
                    page_uuid,
                    transcription_csv_file,
                    i,
                    output_directory,
                    tagged,
                    preprocess_config=preprocess_config,
                    download_images=download_images,
                    txt_line_only=txt_line_only,
                )
                futures.append((transcription_csv_file, future))

        for transcription_csv_file, future in futures:
            try:
                future.result()
            except Exception as e:
                print(f"Error processing {transcription_csv_file.name}: {e}")
                sys.exit()
            progress_bar.set_description(f"Processing {transcription_csv_file.name}")
            # change the colour of the bar based on the status of the future
            progress_bar.update(1)
        else:
            progress_bar.set_description("Processing completed")

        progress_bar.close()
    else:
        for i, (page_uuid, transcription_csv_file) in tqdm(
            enumerate(list(map_page_uuid_csv_file.items()), start=1), desc="Processing pages"
        ):
            # if Path(transcription_csv_file).name != "Registre0007.csv":
            #     continue
            # if Path(transcription_csv_file).name != "Echevronne_1670_1829_0136.csv":
            #     continue
            # print(transcription_csv_file.name)
            # print(transcription_csv_file)
            chosen_function(
                api,
                page_uuid,
                transcription_csv_file,
                i,
                output_directory,
                tagged,
                preprocess_config=preprocess_config,
                download_images=download_images,
            )


def _transcription_with_entities(transcription: str, entities: List) -> str:
    # Sort entities in reverse order by offset
    entities.sort(key=lambda entity: entity.offset, reverse=True)
    test = transcription
    for ent in entities:
        test = test[: ent.offset] + ent.type + "[" + test[ent.offset :]
        end_index = ent.offset + len(ent.type) + ent.length + 1
        test = test[:end_index] + "]" + test[end_index:]

    return html.unescape(test).replace(" ", "·")


# pylint: disable=too-many-locals, disable=too-many-branches, disable=too-many-nested-blocks, disable=line-too-long
def _process_structured_lines_in_transcribus(
    api: ArkIndexAPI,
    page_uuid: str,
    transcription_csv_file: Path,
    page_index: int,
    output_directory: Path,
    tagged: bool,
    image_type: str = "png",
    preprocess_config: Optional[dict] = None,
    download_images: bool = False,
    txt_line_only: bool = False,
) -> None:
    element_json = api.get_element_json_from_element_uuid(page_uuid)
    transcriber_of_line = CSVTranscription(transcription_csv_file)
    classes = [x["ml_class"]["name"] for x in element_json["classifications"]]
    template = [x for x in classes if re.match(r"category_\d", x)][0]
    if txt_line_only:
        lines = api.list_page_text_lines(page_uuid)
    else:
        lines = api.list_all_parts_of_table(page_uuid)

    now = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f+01:00")
    header = f"""
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<PcGts xmlns="http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://schema.primaresearch.org/PAGE/gts/pagecontent/2019-07-15 http://schema.primaresearch.org/PAGE/gts/pagecontent/2019-07-15/pagecontent.xsd">
    <Metadata>
        <Creator>Transkribus</Creator>
        <Created>{now}</Created>
        <LastChange>{now}</LastChange>
        <TranskribusMetadata docId="-1" pageId="-1" pageNr="{page_index}" status="IN_PROGRESS"/>
    </Metadata>
    <Page imageFilename= "{element_json["name"]+ "." + image_type}" imageWidth="{element_json["zone"]["image"]["width"]}" imageHeight="{element_json["zone"]["image"]["height"]}">
        <ReadingOrder>
            <OrderedGroup id="order_group_0" caption="Regions reading order">
"""[
        1:
    ]

    regions = []
    transcriptions = []
    for i, line in enumerate(lines):
        region_type = line["type"]

        if region_type == "text_line":
            x = i - len([x for x in lines[:i] if x["type"] != "text_line"])
            (
                transcription,
                entities,
            ) = transcriber_of_line.format_single_line_transcription_with_entities(
                x, template, **(preprocess_config or {})
            )

        else:
            transcription = ""
            entities = []
        # print()
        # print(_transcription_with_entities(transcription, entities))
        # exit()
        #         columns_len = len(transcription.split(column_separator))
        #         if template in ("category_5", "category_4"):
        #             if columns_len != 12:
        #                 raise ValueError(
        # f"""template - {template} has {columns_len} columns instead of 12 on line {i}
        # {_transcription_with_entities(transcription, entities)}\n""")
        #         else:
        #             if columns_len != 11:
        #                 raise ValueError(
        # f"""template - {template} has {columns_len} columns instead of 11 on line {i}
        # {_transcription_with_entities(transcription, entities)}\n""")

        match region_type:
            case "text_line":
                region_type = "paragraph"
            case "sub_text_line":
                region_type = "paragraph"
            case "above_text_line":
                region_type = "paragraph"
            case "page_header":
                region_type = "header"
            case "table_header":
                region_type = "header"
            case "page_footer":
                region_type = "footer"
            case "comment":
                region_type = "marginalia"
            case _:
                print(f"Unknown region type: {region_type}")
                region_type = "paragraph"
        if tagged:
            transcription = add_tags_to_transcription(transcription, entities)
        # if all you want is text lines get bouding boxes and base lines
        # get bounding box of the polygon
        bounding_box, base_line = get_bounding_box_str(line["zone"]["polygon"])

        # else we need to use the polygons
        polygon = " ".join([f"{x},{y}" for x, y in line["zone"]["polygon"]])

        # we now need to create a string that looks like this: (so it can inserted after
        #  the header) for reading order and store it
        # """
        #         <RegionRefIndexed index="0" regionRef="region_1709821150336_84"/>
        # """
        new_region = f"""
                <RegionRefIndexed index="{i}" regionRef="region_ref_indexed_{i}"/>
""".replace(
            "\n", ""
        )
        regions.append(new_region)

        #
        # now that the regions are handled we need to create the region string
        #  that looks like this:
        # """
        # <TextRegion type="paragraph" id="region_1709821150336_84" custom="readingOrder {index:0;} structure {type:paragraph;}">
        #     <Coords points="156,503 156,622 3128,622 3128,503"/>
        #     <TextLine id="line_1709821150339_87" custom="readingOrder {index:0;}">
        #         <Coords points="156,503 3128,503 3128,622 156,622"/>
        #         <TextEquiv>
        #             <Unicode>This is some text</Unicode>
        #         </TextEquiv>
        #     </TextLine>
        #     <TextEquiv>
        #         <Unicode>This is some text</Unicode>
        #     </TextEquiv>
        # </TextRegion>
        # """
        new_transcription = (
            f"""
        <TextRegion type="paragraph" id="text_region_{i}" custom="readingOrder {{index:{i};}} structure {{type:paragraph;}}">
            <Coords points="{bounding_box}"/>
            <TextLine id="text_line_{i}" custom="readingOrder {{index:{i};}}">
                <Coords points="{bounding_box}"/>
                <Baseline points="{base_line}"/>
                <TextEquiv>
                    <Unicode>{transcription}</Unicode>
                </TextEquiv>
            </TextLine>
            <TextEquiv>
                <Unicode>{transcription}</Unicode>
            </TextEquiv>
        </TextRegion>
"""[
                1:
            ]
            if txt_line_only
            else f"""
        <TextRegion type="paragraph" id="text_region_{i}" custom="readingOrder {{index:{i};}} structure {{type:paragraph;}}">
            <Coords points="{polygon}"/>
            <TextLine id="text_line_{i}" custom="readingOrder {{index:{i};}}">
                <Coords points="{polygon}"/>
                <Baseline points="{base_line}"/>
                <TextEquiv>
                    <Unicode>{transcription}</Unicode>
                </TextEquiv>
            </TextLine>
            <TextEquiv>
                <Unicode>{transcription}</Unicode>
            </TextEquiv>
        </TextRegion>
"""[
                1:
            ]
        )

        transcriptions.append(new_transcription)

    # now we need to put them all together
    regions_text = (
        "\n".join(regions)
        + """
            </OrderedGroup>
        </ReadingOrder>
"""
    )
    header_and_regions = header + regions_text
    transcriptions_text = "".join(transcriptions)

    header_and_regions_and_transcriptions = header_and_regions + transcriptions_text
    header_and_regions_and_transcriptions = (
        header_and_regions_and_transcriptions[:-1]
        + """
    </Page>
</PcGts>
"""[
            :-1
        ]
    )

    # save it as an xml file
    output_file = output_directory / "page" / str(element_json["name"] + ".xml")
    output_file.parent.mkdir(parents=True, exist_ok=True)

    with open(output_file, "w", encoding="utf-8") as f:
        f.write(header_and_regions_and_transcriptions)

    # download the image
    if download_images:
        _download_image_from_arkindex(element_json["zone"]["url"], output_directory, element_json["name"], image_type)


# pylint: disable=too-many-locals, disable=too-many-branches, disable=too-many-nested-blocks, disable=line-too-long, unused-argument
def _process_structured_lines_in_page(
    api: ArkIndexAPI,
    page_uuid: str,
    transcription_csv_file: Path,
    page_index: int,
    output_directory: Path,
    tagged: bool,
    image_type: str = "png",
    preprocess_config: Optional[dict] = None,
    download_images: bool = False,
    txt_line_only: bool = False,
) -> None:
    element_json = api.get_element_json_from_element_uuid(page_uuid)

    classes = [x["ml_class"]["name"] for x in element_json["classifications"]]
    template = [x for x in classes if re.match(r"category_\d", x)][0]
    if txt_line_only:
        lines = api.list_page_text_lines(page_uuid)
    else:
        lines = api.list_all_parts_of_table(page_uuid)
    # lines = order_text_lines_by_vert_center(lines)

    now = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f+01:00")

    header = f"""
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<PcGts xmlns="http://schema.primaresearch.org/PAGE/gts/pagecontent/2019-07-15" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://schema.primaresearch.org/PAGE/gts/pagecontent/2019-07-15 http://schema.primaresearch.org/PAGE/gts/pagecontent/2019-07-15/pagecontent.xsd">
    <Metadata>
        <Creator>Transkribus</Creator>
        <Created>{now}</Created>
        <LastChange>{now}</LastChange>
    </Metadata>
    <Page imageFilename= "../images/{element_json["name"]+ "." + image_type}" imageWidth="{element_json["zone"]["image"]["width"]}" imageHeight="{element_json["zone"]["image"]["height"]}">
"""[
        1:
    ]

    transcriptions = []
    for i, line in enumerate(lines):
        transcriber_of_line = CSVTranscription(transcription_csv_file)
        # the above got messed up because of the introduction of different types of lines
        # I need to identify what type of line it is then use the position of the line to determine if it needs a transcription
        region_type = line["type"]
        if region_type == "text_line":
            x = i - len([x for x in lines[:i] if x["type"] != "text_line"])
            (
                transcription,
                entities,
            ) = transcriber_of_line.format_single_line_transcription_with_entities(
                x, template, **(preprocess_config or {})
            )

        else:
            transcription = ""
            entities = []
        # print()
        # print(_transcription_with_entities(transcription, entities))
        # exit()
        #         columns_len = len(transcription.split(column_separator))
        #         if template in ("category_5", "category_4"):
        #             if columns_len != 12:
        #                 raise ValueError(
        # f"""template - {template} has {columns_len} columns instead of 12 on line {i}
        # {_transcription_with_entities(transcription, entities)}\n""")
        #         else:
        #             if columns_len != 11:
        #                 raise ValueError(
        # f"""template - {template} has {columns_len} columns instead of 11 on line {i}
        # {_transcription_with_entities(transcription, entities)}\n""")

        if tagged:
            transcription = add_tags_to_transcription(transcription, entities)

        # get bounding box of the polygon
        bounding_box, base_line = get_bounding_box_str(line["zone"]["polygon"])
        # else we need to use the polygons
        polygon = " ".join([f"{x},{y}" for x, y in line["zone"]["polygon"]])

        #
        # now that the regions are handled we need to create the region string
        #  that looks like this:
        # """
        # <TextRegion type="paragraph" id="region_1709821150336_84" custom="readingOrder {index:0;} structure {type:paragraph;}">
        #     <Coords points="156,503 156,622 3128,622 3128,503"/>
        #     <TextLine id="line_1709821150339_87" custom="readingOrder {index:0;}">
        #         <Coords points="156,503 3128,503 3128,622 156,622"/>
        #         <TextEquiv>
        #             <Unicode>This is some text</Unicode>
        #         </TextEquiv>
        #     </TextLine>
        #     <TextEquiv>
        #         <Unicode>This is some text</Unicode>
        #     </TextEquiv>
        # </TextRegion>
        # """

        match region_type:
            case "text_line":
                region_type = "paragraph"
            case "sub_text_line":
                region_type = "paragraph"
            case "above_text_line":
                region_type = "paragraph"
            case "page_header":
                region_type = "header"
            case "table_header":
                region_type = "header"
            case "page_footer":
                region_type = "footer"
            case "comment":
                region_type = "marginalia"
            case _:
                print(f"Unknown region type: {region_type}")
                region_type = "paragraph"

        new_transcription = (
            f"""
        <TextRegion type="paragraph" id="text_region_{i}" custom="readingOrder {{index:{i};}} structure {{type:paragraph;}}">
            <Coords points="{bounding_box}"/>
            <TextLine id="text_line_{i}" custom="readingOrder {{index:{i};}}">
                <Coords points="{bounding_box}"/>
                <Baseline points="{base_line}"/>
                <TextEquiv>
                    <Unicode>{transcription}</Unicode>
                </TextEquiv>
            </TextLine>
            <TextEquiv>
                <Unicode>{transcription}</Unicode>
            </TextEquiv>
        </TextRegion>
"""[
                1:
            ]
            if txt_line_only
            else f"""
        <TextRegion type="{region_type}" id="region_ref_indexed_{i}" custom="structure {{type:paragraph;}}">
            <Coords points="{polygon}"/>
            <TextLine id="text_line_{i}" custom="">
                <Coords points="{polygon}"/>
                <TextEquiv>
                    <Unicode>{transcription}</Unicode>
                </TextEquiv>
            </TextLine>
            <TextEquiv>
                <Unicode>{transcription}</Unicode>
            </TextEquiv>
        </TextRegion>
"""[
                1:
            ]
        )

        transcriptions.append(new_transcription)

    header_and_regions = header
    transcriptions_text = "".join(transcriptions)

    header_and_regions_and_transcriptions = header_and_regions + transcriptions_text
    header_and_regions_and_transcriptions = (
        header_and_regions_and_transcriptions[:-1]
        + """
    </Page>
</PcGts>
"""[
            :-1
        ]
    )

    # save it as an xml file
    output_file = output_directory / "xml" / str(element_json["name"] + ".xml")
    output_file.parent.mkdir(parents=True, exist_ok=True)

    with open(output_file, "w", encoding="utf-8") as f:
        f.write(header_and_regions_and_transcriptions)

    # download the image
    if download_images:
        _download_image_from_arkindex(
            element_json["zone"]["url"], output_directory / "images", element_json["name"], image_type
        )


# region Export annotations from Arkindex in COCO format
def export_pares_annotations_from_arkindex_COCO(
    api: ArkIndexAPI,
    output_directory: Path,
    path_to_image_cvs_folder: Path,
    arkindex_image_folder_uuid: str,
    tagged: bool,
    preprocess_config: Optional[dict],
    threaded: bool,
    download_images: bool = False,
) -> None:
    """
    Export annotations from Arkindex in COCO format.
    """

    # Gaining a map of page uuids and csv files so that we can iterate over them
    map_page_uuid_csv_file = map_page_uuids_with_csv_files(api, path_to_image_cvs_folder, arkindex_image_folder_uuid)

    coco_json_dictionary: Dict[str, Any] = {
        "info": {
            "description": "PARES annotations",
            "url": "https://pares.teklia.com/",
            "version": "1.0",
            "year": 2024,
            "contributor": "Guillaume Bernard",
            "date_created": datetime.now(timezone.utc).isoformat(),
        },
        "licenses": [
            {
                "url": "https://creativecommons.org/licenses/by/4.0/",
                "id": 1,
                "name": "Creative Commons Attribution 4.0 International License",
            }
        ],
        "images": [],
        "annotations": [],
        "categories": [
            {
                "id": 1,
                "name": "text_line",
            },
            {
                "id": 2,
                "name": "sub_text_line",
            },
            {
                "id": 3,
                "name": "above_text_line",
            },
            {
                "id": 4,
                "name": "page_header",
            },
            {
                "id": 5,
                "name": "table_header",
            },
            {
                "id": 6,
                "name": "page_footer",
            },
            {
                "id": 7,
                "name": "comment",
            },
        ],
    }
    categories_to_id = {category["name"]: category["id"] for category in coco_json_dictionary["categories"]}  # type: ignore

    if threaded:
        # print the number of threads that will be used to process the pages
        print(f"Using {os.cpu_count()} threads to process the pages")

        with ThreadPoolExecutor() as executor:
            futures = []
            progress_bar = tqdm(total=len(map_page_uuid_csv_file.items()), desc="Processing pages")

            for i, (page_uuid, transcription_csv_file) in enumerate(list(map_page_uuid_csv_file.items()), start=1):
                future1 = executor.submit(
                    _process_structured_lines_in_coco,
                    api,
                    page_uuid,
                    transcription_csv_file,
                    i,
                    output_directory,
                    categories_to_id,
                    tagged,
                    preprocess_config=preprocess_config,
                    download_images=download_images,
                )
                future2 = executor.submit(api.get_element_json_from_element_uuid, page_uuid)

                futures.append((page_uuid, transcription_csv_file, future1, future2))

            for i, (page_uuid, transcription_csv_file, future1, future2) in enumerate(futures, start=1):
                try:
                    element_json = future2.result()
                    coco_json_dictionary["images"].append(
                        {
                            "id": i,
                            "file_name": Path(output_directory / "images" / (element_json["name"] + ".png"))
                            .relative_to(output_directory)
                            .as_posix(),
                            "height": element_json["zone"]["image"]["height"],
                            "width": element_json["zone"]["image"]["width"],
                            "date_captured": datetime.utcnow().isoformat() + "Z",
                        }
                    )

                    annotations = future1.result()
                    coco_json_dictionary["annotations"].extend(annotations)
                except Exception as e:
                    print(f"Error processing {transcription_csv_file.name}: {e}")
                    sys.exit()

                progress_bar.set_description(f"Processing {transcription_csv_file.name}")
                progress_bar.update(1)

            progress_bar.set_description("Processing completed")
            progress_bar.close()
            # since we are using thread that messes up the ids for the annotations
            # lets replace the ids with their positions in the annotations list (make sure it starts at 1)
            for i, annotation in enumerate(coco_json_dictionary["annotations"], start=1):
                annotation["id"] = i
            # al
            # save the coco json
            # even if the folder does not exist, it will be created
            output_directory.mkdir(parents=True, exist_ok=True)

            # Convert the dictionary to a JSON string
            json_string = json.dumps(coco_json_dictionary, ensure_ascii=False, indent=4, sort_keys=False)
            # Strip any trailing whitespace from the JSON string
            cleaned_json_string = json_string.rstrip()
            # Write the cleaned JSON string to the file
            with open(output_directory / "coco.json", "w", encoding="utf-8") as f:
                f.write(cleaned_json_string)

    else:
        for i, (page_uuid, transcription_csv_file) in tqdm(
            enumerate(list(map_page_uuid_csv_file.items()), start=1), desc="Processing pages"
        ):
            # if Path(transcription_csv_file).name != "Registre0007.csv":
            #     continue
            if Path(transcription_csv_file).name != "Echevronne_1670_1829_0136.csv":
                continue

            # lets put the image on the coco json
            element_json = api.get_element_json_from_element_uuid(page_uuid)
            coco_json_dictionary["images"].append(
                {
                    "id": i,
                    "file_name": Path(output_directory / "images" / (element_json["name"] + ".png"))
                    .relative_to(output_directory)
                    .as_posix(),
                    "height": element_json["zone"]["image"]["height"],
                    "width": element_json["zone"]["image"]["width"],
                    "date_captured": datetime.now(timezone.utc).isoformat(),
                }
            )
            # print(transcription_csv_file)
            annotations = _process_structured_lines_in_coco(
                api,
                page_uuid,
                transcription_csv_file,
                i,
                output_directory,
                categories_to_id,
                tagged,
                preprocess_config=preprocess_config,
                download_images=download_images,
            )
            # the extend the dictionary of annotations to the coco json
            coco_json_dictionary["annotations"].extend(annotations)
        # save the coco json
        # even if the folder does not exist, it will be created
        output_directory.mkdir(parents=True, exist_ok=True)
        with open(output_directory / "coco.json", "w", encoding="utf-8") as f:
            json.dump(coco_json_dictionary, f, ensure_ascii=False, indent=4, sort_keys=False)


# pylint: disable=too-many-locals, disable=too-many-branches, disable=too-many-nested-blocks, disable=line-too-long, unnecessary-comprehension
def _process_structured_lines_in_coco(
    api: ArkIndexAPI,
    page_uuid: str,
    transcription_csv_file: Path,
    page_index: int,
    output_directory: Path,
    categories_to_id: Dict,
    tagged: bool,
    image_type: str = "png",
    preprocess_config: Optional[dict] = None,
    download_images: bool = False,
) -> List[Dict[str, Any]]:
    element_json = api.get_element_json_from_element_uuid(page_uuid)

    classes = [x["ml_class"]["name"] for x in element_json["classifications"]]
    template = [x for x in classes if re.match(r"category_\d", x)][0]
    lines = api.list_all_parts_of_table(page_uuid)

    # lines = order_text_lines_by_vert_center(lines)

    # put the images into the coco json, the images will have the same name as transcription_csv_file put as a .png file
    annotations = []
    for i, line in enumerate(lines):
        transcriber_of_line = CSVTranscription(transcription_csv_file)
        # the above got messed up because of the introduction of different types of lines
        # I need to identify what type of line it is then use the position of the line to determine if it needs a transcription
        region_type = line["type"]
        if region_type == "text_line":
            x = i - len([x for x in lines[:i] if x["type"] != "text_line"])
            (
                transcription,
                entities,
            ) = transcriber_of_line.format_single_line_transcription_with_entities(
                x, template, **(preprocess_config or {})
            )

        else:
            transcription = ""
            entities = []

        if tagged:
            transcription = add_tags_to_transcription(transcription, entities)

        bounding_poly = [x for x in line["zone"]["polygon"]]
        # right now it looks like this: [[x1, y1], [x2, y2], [x3, y3], [x4, y4]]
        # we need to make it look like this: [[x1, y1, x2, y2, x3, y3, x4, y4]]
        bounding_poly = [[coord for point in bounding_poly for coord in point]]
        # get the segmentation
        minx, miny, maxx, maxy = map(int, Polygon(line["zone"]["polygon"]).bounds)
        bbox = [minx, miny, maxx - minx, maxy - miny]
        # fill in the annotation dictionary
        annotation = {
            "id": i,
            "image_id": page_index,
            "category_id": categories_to_id[region_type],
            "segmentation": bounding_poly,
            "area": Polygon(line["zone"]["polygon"]).area,
            "bbox": bbox,
            "iscrowd": 0,
            "text": transcription,
        }
        annotations.append(annotation)

    # download the image
    if download_images:
        _download_image_from_arkindex(
            element_json["zone"]["url"], output_directory / "images", element_json["name"], image_type
        )
    # what is the Typing type of annotations
    return annotations


def get_bounding_box_str(polygon: List[List[int]]) -> tuple[str, str]:
    """
    Get the bounding box of a polygon using shapely.
    """
    # Convert the polygon coordinates to a shapely Polygon object
    poly = Polygon(polygon)

    # Get the bounding box coordinates
    minx, miny, maxx, maxy = map(int, poly.bounds)

    # Format the bounding box coordinates as a string
    bounding_box_str = f"{minx},{miny} {maxx},{miny} {maxx},{maxy} {minx},{maxy}"

    # The baseline should be the bottom of the bounding box
    base_line = f"{minx},{maxy} {maxx},{maxy}"

    return bounding_box_str, base_line


def add_tags_to_transcription(transcription: str, entities: List[Entity]) -> str:
    # Sort entities in reverse order by offset
    entities.sort(key=lambda entity: entity.offset, reverse=True)
    for entity in entities:
        # Insert end tag
        tag = sanitize_xml_tag(f"<{entity.type}>")

        # split the transcription by spaces from the offset
        space_split = transcription[entity.offset : entity.offset + entity.length].split(" ")

        # make new entities based on this split in a temporary list
        temp_entities = []

        new_offset = entity.offset

        for split in space_split:
            temp_entities.append(
                Entity(
                    name=split,
                    type=entity.type,
                    offset=new_offset,
                    length=len(split),
                )
            )
            new_offset += len(split) + 1
        # sort the new entities by offset in reverse order
        temp_entities.sort(key=lambda entity: entity.offset, reverse=True)
        # add the new entities to the transcription
        for temp_entity in temp_entities:
            transcription = _insert_string(transcription, temp_entity.offset + temp_entity.length, tag)

    return transcription


def sanitize_xml_tag(tag):
    # slice the tag to remove the first and last characters
    tag_body = tag[1:-1]
    # replace any non-word character or hyphen with underscore
    tag_body = unidecode(tag_body)
    sanitized_tag_body = re.sub(r"[^\w-]", "_", tag_body)
    # the characters trailing and leading the tag cannot be _
    sanitized_tag_body = re.sub(r"^_|_$", "", sanitized_tag_body)
    # add the first and last characters back to the sanitized tag
    sanitized_tag = html.escape("<" + sanitized_tag_body + ">")
    return sanitized_tag


def _insert_string(string: str, position: int, insertion: str) -> str:
    return string[:position] + insertion + string[position:]


def _download_image_from_arkindex(url: str, output_directory: Path, name: str, image_type: str) -> None:
    """
    Download the image from Arkindex and store it in the output directory.
    """
    # download the image using the url and store it in the output directory

    image = get_image(url)
    # at the url there is just an image, so we can just save it as a .png file
    output_path = output_directory / (str(name) + "." + image_type)
    output_path.parent.mkdir(parents=True, exist_ok=True)

    with output_path.open("wb") as f:
        f.write(image.content)


@retry(stop=stop_after_attempt(3), wait=wait_fixed(1), retry=retry_if_exception_type(ChunkedEncodingError))
def get_image(url):
    return requests.get(url)


# endregion
