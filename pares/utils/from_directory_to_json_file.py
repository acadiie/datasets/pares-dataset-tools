#!/usr/bin/env python3

#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

import argparse
import json
from pathlib import Path


def main():
    parser = argparse.ArgumentParser(
        description="Convert a directory hierarchy of multiple templates into a JSON file. "
        "This read the children directory of the one given in parameter and converts the list of "
        "files into a JSON format."
    )
    parser.add_argument(
        "--input-directory",
        help="The directory hierarchy to export.",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--output",
        help="Name of the JSON file to export the list of elements to.",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    input_directory = Path(args.input_directory)
    output_file = args.output

    files_in_templates = {}
    for template_directory in input_directory.iterdir():
        template_directory_path = Path(template_directory)
        images = [image.name for image in template_directory_path.iterdir()]
        files_in_templates[template_directory.name] = {
            "count": len(images),
            "images": images,
        }
        with open(output_file, mode="w", encoding="utf-8") as of:
            json.dump(files_in_templates, of, indent=2)

    # Consistency checks
    total_count = sum(item.get("count", 0) for key, item in files_in_templates.items())
    print(f"- Total count: {total_count} images.")

    all_images = [item.get("images", []) for key, item in files_in_templates.items()]
    intersection = set.intersection(*map(set, all_images))
    if len(intersection) == 0:
        print("- No intersection in templates (no duplicate images in different directories).")
    else:
        raise ValueError(f"An intersection was found ({intersection} are duplicated).")


if __name__ == "__main__":
    main()
