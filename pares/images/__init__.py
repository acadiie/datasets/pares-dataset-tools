#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

import argparse
from pathlib import Path

from pares.images.extract_from_labels import (
    extract_segmentation_from_images_using_label_masks,
)
from pares.images.resize import resize_images


def main():
    parser = argparse.ArgumentParser("Convert, extract elements from images and labels.")
    parser.add_argument(
        "--images",
        "-i",
        help="Directory with images or labels, depending on the subcommand to use.",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--output-directory", "-o", type=str, required=True, help="Directory where to export the images."
    )

    subparsers = parser.add_subparsers(dest="actions")
    subparser_extract_segmentation = subparsers.add_parser(
        "extract-segmentation", help="Extract one element of a segmentation image based on its color."
    )

    subparser_extract_segmentation.add_argument(
        "--labels", "-l", help="Directory with segmentation labels", type=str, required=True
    )
    subparser_extract_segmentation.add_argument(
        "--type-of-element-extract",
        help="The type of element to extract, as present in the JSON label files.",
        type=str,
        required=True,
    )
    subparser_resize = subparsers.add_parser(
        "resize",
        help="Resize (for most of usages, downsize) images of a directory to a new height. It is possible to "
        "add zero padding on the left or right depending on whether you wish to get a square or to keep "
        "the original aspect ratio.",
    )
    subparser_resize.add_argument("--new-height", type=int, required=True, help="New height for the images.")
    subparser_resize.add_argument(
        "--add-padding",
        action="store_true",
        help="Whether or not to add padding on the sides of the images.",
    )

    args = parser.parse_args()

    images_path = Path(args.images)
    labels_path = Path(args.labels)
    all_images = list(images_path.iterdir())
    all_labels = list(labels_path.iterdir())

    output_directory = Path(args.output_directory)
    output_directory.mkdir(parents=True, exist_ok=True)

    match args.actions:
        case "extract-segmentation":
            extract_segmentation_from_images_using_label_masks(
                all_labels, all_images, args.type_of_element_extract, output_directory
            )
        case "resize":
            resize_images(images_path, all_images, output_directory, args.new_height, args.add_padding)
