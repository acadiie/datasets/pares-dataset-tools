#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

"""
Resize images (generally downsize) to fix neural networks.
"""

import io
import subprocess
from pathlib import Path
from typing import List

from PIL import Image
from tqdm import tqdm


def resize_image_with_padding(image_path: Path, new_height: int = 768) -> Image:
    """
    Resize the image, and add padding on left and right if the image is not a perfect square, in order to keep its
    original aspect ratio.
    """
    with subprocess.Popen(
        f"""convert \
          -morphology Erode Square:2 -interpolate Nearest -filter point \
          -resize x{new_height} -gravity center \
          -extent "{new_height}x{new_height}" \
          -background black \
          "{image_path}" png:-""",
        shell=True,
        stdout=subprocess.PIPE,
    ) as process:
        process.wait()
        stdout, _ = process.communicate()
        return Image.open(io.BytesIO(stdout))


def resize_image_without_padding(image_path: Path, new_height: int = 768) -> Image:
    """
    Resize the image and keep the original aspect ratio.
    """
    with subprocess.Popen(
        f"""convert \
          -morphology Erode Square:2 -interpolate Nearest -filter point \
          -resize x{new_height} -gravity center \
          "{image_path}" png:-""",
        stdout=subprocess.PIPE,
        shell=True,
    ) as process:
        process.wait()
        stdout, _ = process.communicate()
        return Image.open(io.BytesIO(stdout))


def resize_images(
    images_path: Path, all_images: List[Path], output_directory: Path, new_height: int, add_padding: bool
) -> None:
    output_directory.mkdir(exist_ok=True, parents=True)
    for image_path in tqdm(
        all_images,
        desc=f"Resizing images from ‘{images_path.name}’ ({new_height}px in height − padding: {add_padding})",
    ):
        if add_padding:
            image = resize_image_with_padding(image_path, new_height)
        else:
            image = resize_image_without_padding(image_path, new_height)
        image.save(output_directory / image_path.name)
