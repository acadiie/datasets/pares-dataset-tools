#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

"""
Extract text lines from images using label masks.
"""

import json
from pathlib import Path
from typing import List

import cv2
import numpy
from PIL import Image
from shapely import Polygon
from tqdm import tqdm


def extract_polygons_from_original_image(image: Image, polygons: List[Polygon]) -> List[Image]:
    """
    Finds the connected components of an image using a list of polygons.

    :return: all the extracted components from the image, based on the polygons.
    """
    image_array = numpy.asarray(image).astype("uint8")

    components = []
    for polygon in polygons:
        # Convert Polygon object to coordinates for the fillPoly function.
        points = numpy.array([[int(x), int(y)] for x, y in zip(*polygon.boundary.coords.xy)])

        # We create an image in which only the connected component will remain. It’s RGB.
        text_line_only_image_array = numpy.zeros_like(image_array, dtype=numpy.uint8)
        cv2.fillPoly(text_line_only_image_array, [points], (255, 255, 255))

        # Performing bitwise operation to remove any part if not inside this contour
        extracted_from_image = numpy.bitwise_and(image_array, text_line_only_image_array)

        # Extract bounding rectangle from the extracted image
        min_x, min_y, max_x, max_y = (int(i) for i in polygon.bounds)
        extracted_from_image = extracted_from_image[min_y:max_y, min_x:max_x]
        components.append(Image.fromarray(extracted_from_image))
    return components


def load_image_and_keep_only_element_of_a_given_type(label_path: Path, type_of_element_to_keep: str) -> List[Polygon]:
    """
    Load the label_json file and extract of the polygons of the given type.

    :param label_path: path to the image to load
    :param type_of_element_to_keep: the class of element to keep, present in the JSON label file.
    """
    with open(label_path, mode="r", encoding="utf-8") as i_json_file:
        annotations = json.load(i_json_file)
        polygons = [Polygon(item.get("polygon")) for item in annotations.get(type_of_element_to_keep)]
        return sorted(polygons, key=lambda polygon: polygon.centroid.y)


def resize_image_to_maximum_height(image: Image, new_height_in_px: int = 128) -> Image:
    """
    Resize an image, keeping its original aspect ratio, to a maximum height given in parameter.
    """
    new_width = int(new_height_in_px * image.width / image.height)
    return image.resize((new_width, new_height_in_px), Image.LANCZOS)


def extract_segmentation_from_images_using_label_masks(
    all_labels_json: List[Path],
    all_images: List[Path],
    type_of_element_to_keep: str,
    output_directory: Path,
) -> None:
    """
    Extract the elements from images using labels. The element to extract has to be represented by a unique
    color in all masks. This color will be considered as the element to extract and the coordinates found in the
    mask and, using these coordinates, the element is extracted from the corresponding image.

    This function directly outputs the text line in the output directory given in parameter, using the same
    name as the original image, appending ‘_text_line_x` as suffix.
    """
    for image_path, label_path in tqdm(
        zip(all_images, all_labels_json), desc="Extracting text lines from images…", total=len(all_labels_json)
    ):
        polygons_to_extract = load_image_and_keep_only_element_of_a_given_type(label_path, type_of_element_to_keep)
        text_lines = extract_polygons_from_original_image(Image.open(image_path), polygons_to_extract)
        for i, text_line in enumerate(text_lines, start=1):
            resized_text_line = resize_image_to_maximum_height(text_line)
            resized_text_line.save(output_directory / f"{image_path.stem}_text_line_{i}.png")
