#!/usr/bin/env python3

#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

import argparse
import json
import random
import shutil
from pathlib import Path

import pandas as pd


# pylint: disable=too-many-locals
def main():
    parser = argparse.ArgumentParser(
        "Split a dataset (previously exported) to train/test/validation subsets using constraints."
    )

    parser.add_argument(
        "--input-template-file",
        help="The JSON file with the name of the classes and the list of image files that belong to this class. "
        "It is the document exported by the from_directory_architecture_to_json_file.py script.",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--input-statistics",
        help="The CSV file that indicates how many images from each template should go to train, test or validation.",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--input-images-directory",
        help="The directory that contains ’images’ and ’labels’ sub-folders that we will be split by this script.",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--output-path",
        help="Where to put the newly created dataset of images and labels.",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--random-seed", help="Seed for the random number generator", type=int, required=False, default=0
    )

    args = parser.parse_args()
    input_template_file = Path(args.input_template_file)
    input_statistics = Path(args.input_statistics)
    input_images_directory = Path(args.input_images_directory)
    output_path = Path(args.output_path)

    # Set the random seed so the results will be reproducible
    random_seed = args.random_seed
    random.seed(random_seed)

    # Create the output directory hierarchy
    output_path.mkdir(exist_ok=True, parents=True)
    for export_type in ["train", "val", "test"]:
        (output_path / export_type).mkdir(exist_ok=True, parents=True)
        for dir_name in ["images", "labels"]:
            (output_path / export_type / dir_name).mkdir(exist_ok=True, parents=True)

    # Open configuration files
    train_test_val_split_statistics = pd.read_csv(input_statistics, header=None)
    with open(input_template_file, mode="r", encoding="utf-8") as i_file:
        templates_classes = json.load(i_file)

        # And distribute images and labels to their destination
        already_distributed_images = set()
        for _, (template_name, partition, count) in train_test_val_split_statistics.iterrows():
            images_in_template = [
                image
                for image in templates_classes.get(template_name, {}).get("images", [])
                if image not in already_distributed_images
            ]
            sample = random.sample(images_in_template, count)

            for image_name in sample:
                image_path = input_images_directory / "images" / image_name
                label_path = input_images_directory / "labels" / image_name

                image_path_destination = output_path / partition / "images" / image_name
                label_path_destination = output_path / partition / "labels" / image_name

                shutil.copy(image_path, image_path_destination)
                shutil.copy(label_path, label_path_destination)

            already_distributed_images.update(sample)


if __name__ == "__main__":
    main()
