#!/usr/bin/env python3

#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

import argparse
import glob
from pathlib import Path
from shutil import copy2
from typing import List

from sklearn.model_selection import train_test_split


# pylint: disable=too-many-arguments,too-many-locals
def train_test_validation_split(
    train_size: int,
    val_size_from_test_ds: float,
    files_to_sample: List[str],
    random_state: int,
    input_directory: Path,
    output_directory: Path,
):
    X_train, X_test = train_test_split(files_to_sample, train_size=train_size, random_state=random_state)
    X_val, X_test = train_test_split(X_test, train_size=val_size_from_test_ds, random_state=random_state)

    for subset_name, subset in [("train", X_train), ("test", X_test), ("val", X_val)]:
        for file in subset:
            filename = Path(file)
            base_filename = filename.stem
            subdirs = [("images", ".png"), ("labels", ".png"), ("labels_json", ".json")]

            for subdir, extension in subdirs:
                file_old_path = Path(input_directory) / subdir / f"{base_filename}{extension}"
                new_path_base = Path(output_directory) / subset_name / subdir
                new_path_base.mkdir(parents=True, exist_ok=True)
                file_new_path = new_path_base / f"{base_filename}{extension}"
                copy2(file_old_path, file_new_path)


def main():
    parser = argparse.ArgumentParser("Split a dataset (previously exported) to train/test/validation subsets.")
    parser.add_argument("--input-directory", "-i", help="Path to the dataset", type=str, required=True)
    parser.add_argument(
        "--output-directory", "-o", help="Path where the splits will be written to.", type=str, required=True
    )
    parser.add_argument(
        "--train-split",
        help="""Value between 0 and 1 where to split in train and test.
        For instance, if 0.8, 80 percent of the corpus will be in train (default: 0.8)""",
        type=float,
        metavar="[0, 1]",
        default=0.8,
        required=False,
    )
    parser.add_argument(
        "--val-size-from-test-size",
        help="Portion, between 0 and 1, of the test split that will be dedicated to validation. (default: 0.5)",
        default=0.5,
        required=False,
    )
    parser.add_argument(
        "--random-state", help="Value of the random state (default: 0)", type=int, required=False, default=0
    )
    args = parser.parse_args()

    files_to_sample = sorted(glob.glob(f"{args.input_directory}/images/*.png"))

    train_test_validation_split(
        args.train_split,
        args.val_size_from_test_size,
        files_to_sample,
        args.random_state,
        args.input_directory,
        args.output_directory,
    )
