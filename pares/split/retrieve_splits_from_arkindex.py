#!/usr/bin/env python3

#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

import argparse
import json
from typing import Dict, List

import numpy
from sklearn.model_selection import train_test_split

from pares.common import read_config_file_json
from pares.common.arkindex import ArkIndexAPI


# pylint: disable=too-many-arguments
def get_splits(
    api: ArkIndexAPI,
    arkindex_image_folder_id: str,
    random_state: int,
    train_size: float,
    val_test_size: float,
    min_val_test: int,
):
    """ """
    pages = api.list_element_children_with_classes(arkindex_image_folder_id)

    (
        categories_in_train,
        categories_in_test,
        categories_in_validation,
    ) = get_category_counts_for_each_subset_train_test_and_validation(
        pages, min_val_test, train_size, val_test_size, random_state
    )

    # Extract page that are in train, based on categories
    train_pages = retrieve_pages(categories_in_train, pages, random_state)
    # Remove pages that are already used in train to avoid duplicates in test and validation
    pages = remove_pages_from_first_list_if_present_in_second_list(pages, train_pages)

    # Repeat for the test set and validation set
    test_pages = retrieve_pages(categories_in_test, pages, random_state)
    pages = remove_pages_from_first_list_if_present_in_second_list(pages, test_pages)
    validation_pages = retrieve_pages(categories_in_validation, pages, random_state)

    # Check that there is no duplicate in train, test and validation
    check_non_overlapping_elements(train_pages, test_pages, validation_pages)

    with open("classes.json", mode="w", encoding="utf-8") as f:
        json.dump(merge_train_test_validation_splits_into_single_dict(train_pages, test_pages, validation_pages), f)


def get_category_counts_for_each_subset_train_test_and_validation(
    pages: List[Dict],
    min_val_test: int,
    train_size: float,
    val_test_size: float,
    random_state: int = 0,
):
    """
    Distribute the categories between the train, test and validation subsets.
    It extracts the categories of each page (‘category_1’, ‘category_2’…) and create splits in order to have the low
    represented categories in the validation and test subsets. This way, we create a split that will put
    underrepresented classes in test and validation, putting the rest in training. In the end, this will be useful
    for training a model that will be tested on categories rare in train.

    :param pages: the list of Page dictionaries returned by the ArkIndexAPI.
    :param min_val_test: the minimum size of the validation and test sets when creating the train test split
    :param train_size: the size, as a ratio (0 < size < 1) of the training set
    :param val_test_size: the size, as a ratio (0 < size < 1) of the validation and test sets
    :param random_state: seed to initialise random values

    :return: a tuple containing, in this order, a numpy array for the train, test and validation split. It’s a 2D array
             with the category name on the first line, the number of elements in this category in the second line.
             ie: (['category_1' 'category_2' 'category_3'], [31  1  17])
    """
    # FIXME: this is not robust because of explicit ’category_’ and reference to the first index
    classes = [get_arkindex_classes_of_a_page(page, keep_only_starting_with="category_")[0] for page in pages]

    to_test_1, whole = limit_min(classes, min_val_test, train_size, val_test_size)
    train, test = train_test_split(whole, train_size=train_size, stratify=whole, random_state=random_state)

    to_test_2, test = limit_min(test, min_val_test, val_test_size)
    test, validation = train_test_split(test, test_size=0.5, stratify=test, random_state=random_state)

    test = numpy.concatenate((test, to_test_1, to_test_2))
    return (
        numpy.unique(train, return_counts=True),
        numpy.unique(test, return_counts=True),
        numpy.unique(validation, return_counts=True),
    )


def limit_min(array: List[str], amount, split_size, next_split: float = 1.0):
    """
    This function will take an array and remove all values that have less than amount
    returning the removed values and the new array

    The function ensures that the array wil be split without errors based on
    a small number of class values in the array

    Used to ensure more generalized splits

    array: the array to be split
    amount: the minimum number of values that must be in the array
    split_size: the size of the split that will be done to the array
    next_split: the size of the next split that will be done to the array (such as the test/validation split)
    """
    array = numpy.array(array)
    counts = numpy.unique(array, return_counts=True)
    small_counts = numpy.where(counts[1] * split_size * next_split < amount)
    to_test = []
    for count in small_counts[0]:
        val = counts[0][count]
        count = counts[1][count]
        to_test.extend([val] * count)
        array = array[array != val]
    to_test = numpy.array(to_test)
    array = numpy.sort(array[~numpy.isin(array, to_test)])
    return to_test, array


def retrieve_pages(category_counts: numpy.array, pages: List[Dict], random_state: int) -> List:
    """
    Retrieve the Pages (as the form of dictionaries) following the categories given in the split.
    """
    selected_pages = []
    for category_name, category_nb_of_elements in zip(category_counts[0], category_counts[1]):
        all_pages_of_this_category = [page for page in pages if category_name in get_arkindex_classes_of_a_page(page)]
        numpy.random.RandomState(seed=random_state).shuffle(all_pages_of_this_category)
        selected_pages.extend(all_pages_of_this_category[:category_nb_of_elements])
    return selected_pages


def get_arkindex_classes_of_a_page(page: Dict, keep_only_starting_with: str = "") -> List[str]:
    """
    Get the ArkIndex classes for a single page.
    If the keep_only_starting_with parameter is filled, is will only select classes starting with the string given.

    :return: the list of classes of the page
    """
    classes = []
    for ml_class in page.get("classes", []):
        class_name = ml_class.get("ml_class", {}).get("name")
        if keep_only_starting_with != "":
            if class_name.startswith(keep_only_starting_with):
                classes.append(class_name)
        else:
            classes.append(class_name)
    return classes


def remove_pages_from_first_list_if_present_in_second_list(
    first_list: List[Dict], second_list: List[Dict]
) -> List[Dict]:
    """
    Remove the pages of the first list if they are present in the second list.
    """
    pages_ids_to_remove = [page.get("id") for page in second_list]
    return [page for page in first_list if page.get("id") not in pages_ids_to_remove]


def check_non_overlapping_elements(train_pages: List[Dict], test_pages: List[Dict], validation_pages: List[Dict]):
    """Ensure there is no overlap between the train, test and validation pages."""
    train_ids = set(page.get("id") for page in train_pages)
    test_ids = set(page.get("id") for page in test_pages)
    validation__ids = set(page.get("id") for page in validation_pages)
    assert len(train_ids.intersection(test_ids)) == 0
    assert len(train_ids.intersection(validation__ids)) == 0
    assert len(test_ids.intersection(validation__ids)) == 0


def merge_train_test_validation_splits_into_single_dict(train_pages, test_pages, validation_pages):
    return (
        {
            "train": _return_dict_from_single_split(train_pages),
            "test": _return_dict_from_single_split(test_pages),
            "validation": _return_dict_from_single_split(validation_pages),
        },
    )


def _return_dict_from_single_split(single_split: List[Dict]):
    return [
        {
            "id": page.get("id"),
            "name": page.get("name"),
            "category": get_arkindex_classes_of_a_page(page, keep_only_starting_with="category_")[0],
        }
        for page in single_split
    ]


def main():
    parser = argparse.ArgumentParser(
        description="Create train, test and validation splits from an ArkIndex repository, based on the"
        "classification of each element. Each image in the folder on ArkIndex must have a category "
        "starting with ‘category_X’, X being an integer."
    )
    parser.add_argument(
        "--config-file",
        help="Path to the JSON file containing the configuration to export the files",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--seed", help="The random state to use for the train test split", type=int, required=False, default=42
    )
    parser.add_argument("--train-size", help="The size of the training set", type=float, required=False, default=0.8)
    parser.add_argument(
        "--val-test-size", help="The size of the validation and test sets", type=float, required=False, default=0.5
    )
    parser.add_argument(
        "--min-val-test",
        help="The minimum size of the validation and test sets when creating the train test split",
        type=int,
        required=False,
        default=1,
    )
    args = parser.parse_args()
    api = ArkIndexAPI.create_from_environment()
    config = read_config_file_json(args.config_file)
    get_splits(
        api,
        config.get("arkindex_image_folder_id"),
        args.seed,
        args.train_size,
        args.val_test_size,
        args.min_val_test,
    )


if __name__ == "__main__":
    main()
