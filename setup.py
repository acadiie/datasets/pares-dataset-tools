#!/usr/bin/env python3

#  SPDX-FileCopyrightText: 2023 Guillaume Bernard <contact@guillaume-bernard.fr>
#  SPDX-FileCopyrightText: 2023 Casey Wall <casey.wall@univ-lr.fr>
#  SPDX-License-Identifier: MIT

import datetime
import re
from typing import List

from setuptools import find_packages, setup


def get_requirements() -> List[str]:
    req = list()
    with open("requirements.txt") as requirements:
        pattern = re.compile(r"^.*#egg=([\w]+)$")
        for line in requirements.read().splitlines():
            if pattern.match(line):
                req.append(pattern.findall(line)[0])
            else:
                req.append(line)
    return req


# noinspection PyUnresolvedReferences
setup(
    version=f"1.0.post{datetime.datetime.now().strftime('%Y%m%d%H%M%S')}",
    packages=find_packages(),
    install_requires=get_requirements(),
)
