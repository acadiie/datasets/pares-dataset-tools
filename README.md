# PARES Dataset Tools

This project centralises the tools to manipulate the [PARES Dataset (PArish REcord Survey)](https://zenodo.org/records/8337504).

![Python 3.10](https://img.shields.io/badge/Python_Version-3.10-success)

## Command line tools

### `pares-export-from-arkindex`

```bash
usage: Export annotations from ArkIndex (Teklia software) [-h] --output-directory OUTPUT_DIRECTORY --config-file CONFIG_FILE

options:
  -h, --help            show this help message and exit
  --output-directory OUTPUT_DIRECTORY, -o OUTPUT_DIRECTORY
                        Where to store the exported files
  --config-file CONFIG_FILE
                        Path to the JSON file containing the configuration to export the files
```

### `pares-arkindex`

```bash
usage: pares-arkindex [-h] --config-file CONFIG_FILE {transcriptions,entities,categories} ...

Create, delete transcriptions, entities or categories on ArkIndex.

positional arguments:
  {transcriptions,entities,categories}
    transcriptions      Create, delete transcriptions.
    entities            Create, delete entities.
    categories          Create, delete categories.

options:
  -h, --help            show this help message and exit
  --config-file CONFIG_FILE
                        Path to the JSON file containing the configuration to export the files
```

### `pares-checks`

```bash
usage: pares-checks [-h] --config-file CONFIG_FILE {intersections,transcriptions} ...

Perform check on the PARES dataset on an ArkIndex instance.

positional arguments:
  {intersections,transcriptions}
    intersections       Ensure that there is no overlap between polygons.
    transcriptions      Ensure that the number of transcription matches the number of text_lines.

options:
  -h, --help            show this help message and exit
  --config-file CONFIG_FILE
                        Path to the JSON file containing the configuration to export the files
```

### `pares-images`

```bash
usage: Convert, extract elements from images and labels. [-h] --images IMAGES {extract-segmentation,resize} ...

positional arguments:
  {extract-segmentation,resize}
    extract-segmentation
                        Extract one element of a segmentation image based on its color.
    resize              Resize (for most of usages, downsize) images of a directory to a new height. It is possible to add zero padding on the left or right depending on whether you wish to get a square or to keep the original aspect ratio.

options:
  -h, --help            show this help message and exit
  --images IMAGES, -i IMAGES
                        Directory with images or labels, depending on the subcommand to us
```

### `pares-splits`

#### `pares-random-split`

```bash
usage: Split a dataset (previously exported) to train/test/validation subsets. [-h] --input-directory INPUT_DIRECTORY --output-directory OUTPUT_DIRECTORY [--train-split [0, 1]] [--val-size-from-test-size VAL_SIZE_FROM_TEST_SIZE] [--random-state RANDOM_STATE]

options:
  -h, --help            show this help message and exit
  --input-directory INPUT_DIRECTORY, -i INPUT_DIRECTORY
                        Path to the dataset
  --output-directory OUTPUT_DIRECTORY, -o OUTPUT_DIRECTORY
                        Path where the splits will be written to.
  --train-split [0, 1]  Value between 0 and 1 where to split in train and test. For instance, if 0.8, 80 percent of the corpus will be in train (default: 0.8)
  --val-size-from-test-size VAL_SIZE_FROM_TEST_SIZE
                        Portion, between 0 and 1, of the test split that will be dedicated to validation. (default: 0.5)
  --random-state RANDOM_STATE
                        Value of the random state (default: 0)
```

#### `pares-stratified-split`

```bash
usage: Split a dataset (previously exported) to train/test/validation subsets using constraints. [-h] --input-template-file INPUT_TEMPLATE_FILE --input-statistics INPUT_STATISTICS --input-images-directory INPUT_IMAGES_DIRECTORY --output-path OUTPUT_PATH [--random-seed RANDOM_SEED]

options:
  -h, --help            show this help message and exit
  --input-template-file INPUT_TEMPLATE_FILE
                        The JSON file with the name of the classes and the list of image files that belong to this class. It is the document exported by the from_directory_architecture_to_json_file.py script.
  --input-statistics INPUT_STATISTICS
                        The CSV file that indicates how many images from each template should go to train, test or validation.
  --input-images-directory INPUT_IMAGES_DIRECTORY
                        The directory that contains ’images’ and ’labels’ sub-folders that we will be split by this script.
  --output-path OUTPUT_PATH
                        Where to put the newly created dataset of images and labels.
  --random-seed RANDOM_SEED
                        Seed for the random number generator
```

#### `pares-retrieve-splits-from-arkindex`

```bash
usage: pares-retrieve-splits-from-arkindex [-h] --config-file CONFIG_FILE [--seed SEED] [--train-size TRAIN_SIZE] [--val-test-size VAL_TEST_SIZE] [--min-val-test MIN_VAL_TEST]

Create train, test and validation splits from an ArkIndex repository, based on theclassification of each element. Each image in the folder on ArkIndex must have a category starting with ‘category_X’, X being an integer.

options:
  -h, --help            show this help message and exit
  --config-file CONFIG_FILE
                        Path to the JSON file containing the configuration to export the files
  --seed SEED           The random state to use for the train test split
  --train-size TRAIN_SIZE
                        The size of the training set
  --val-test-size VAL_TEST_SIZE
                        The size of the validation and test sets
  --min-val-test MIN_VAL_TEST
                        The minimum size of the validation and test sets when creating the train test split
```

#### `pares-analysis`

```bash
usage: pares-analysis [-h] --config-file CONFIG_FILE --output-folder OUTPUT_FOLDER
                      [--path_to_json PATH_TO_JSON]
                      {pix_stats,trans_stats,show_counter} ...

Output statistics that describe the PARES dataset.

positional arguments:
  {pix_stats,trans_stats,show_counter}
    pix_stats           Output statistics and images about the annotations, pixel-wise, of
                        the images.
    trans_stats         Output statistics about the transcriptions, character and word-wise,
                        of the images
    show_counter        Saves an image of the counter at the path given by the user (can
                        only be run after trans_stats)

options:
  -h, --help            show this help message and exit
  --config-file CONFIG_FILE
                        Path to the JSON file containing the configuration to export the
                        files
  --output-folder OUTPUT_FOLDER
                        Path to the folder where to output the stats
  --path_to_json PATH_TO_JSON
                        Path to the json file containing the counter dict (only can be used
                        after trans_stats has be run)
```

## Configuration

### `.env`

```dotenv
ARKINDEX_API_URL="https://XXX.arkindex.org"
ARKINDEX_API_TOKEN="SECRET_TOKEN"
# Might be helpful if the IIIF server is slow
IMAGEIO_REQUEST_TIMEOUT=20
# To create some elements on ArkIndex, a worker_id is required.
# It is specific to the user’s account and unique for each project
ARKINDEX_USER_WORKER_UUID=""
# To create some elements on ArkIndex, a worker_id is required.
# It is specific to the user’s account and unique for each project
# "26f8de57-7185-447f-b36c-ea34dd5cde86" is for a unstructured transcriptions
# "de5123c0-078f-4c89-9feb-5dbe4a3c8e2f" is for a structured transcriptions
# "fc708f2b-f6da-4fad-bc92-22fad77ba5d1" is the old worker
ARKINDEX_USER_WORKER_UUID="de5123c0-078f-4c89-9feb-5dbe4a3c8e2f"
```

### `config.json`

```json
{
  "arkindex_image_folder_id": "",
  "arkindex_image_folder_name": "",
  "categories_uuids": {
    "category_1": "",
    "category_2": "",
    "category_3": "",
    "category_4": "",
    "category_5": "",
    "category_6": "",
    "category_7": ""
  },
  "classes": {
    "above_text_line": "255,120,0",
    "comment": "237,51,59",
    "page_footer": "102,178,255",
    "page_header": "184,40,158",
    "sub_text_line": "248,228,92",
    "table_header": "255,51,153",
    "text_line": "0,153,0"
  },
  "splits_uuid": {
    "train":"",
    "test":"",
    "validation":""
    },
  "entity_names": ["", ...],
  "corpus_uuid": ""
  "transcriptions_path": "../path/to/dir/with/transcriptions"
}
```
